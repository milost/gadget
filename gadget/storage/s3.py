# -*- coding: utf-8 -*-
"""This module contains the implementation of a S3 storage backend."""
import io
from collections import defaultdict
from pathlib import Path
from typing import Iterable
from typing import List
from typing import Optional
from typing import Union

from minio import Minio
from minio.datatypes import Bucket
from minio.deleteobjects import DeleteObject
from minio.error import S3Error
from minio.sse import SseCustomerKey
from plum import dispatch
from urllib3.exceptions import ResponseError

from .common import compute_stream_length
from gadget.models import ArtifactLocation
from gadget.storage.storage import Storage

# from minio.commonconfig import Tags

# pylint: disable=too-many-arguments,function-redefined,arguments-renamed


class S3Storage(Storage):
    """This class implements the S3 object storage backend.

    Examples:
            # Connect to MinIO Server
            storage = MinIOStorage('localhost:9000',
                      access_key='YOUR-ACCESSKEYID',
                      secret_key='YOUR-SECRETACCESSKEY',
                      secure=True)
            storage.connect()

            # Connect to AWS S3
            storage = MinIOStorage('s3.amazonaws.com',
                      access_key='YOUR-ACCESSKEYID',
                      secret_key='YOUR-SECRETACCESSKEY',
                      secure=True)
            storage.connect()
    """
    def __init__(self,
                 host: str = 'localhost',
                 port: int = 9000,
                 access_key: str = None,
                 secret_key: str = None,
                 secure: bool = True) -> None:
        """Constructor of the S3 storage backend.

        Args:
            host: host of object storage service.
            port: port of object storage service.
            access_key: Access key is like user ID that uniquely identifies your account.
            secret_key: Secret key is the password to your account.
            secure: Set this value to 'True' to enable secure (HTTPS) access. (default: True)
        """
        self.host = host
        self.port = port
        self.access_key = access_key
        self.secret_key = secret_key
        self.secure = secure
        self.connection: Optional[Minio] = None

    def connect(self) -> Minio:
        """Establish a connection to the MinIO storage backend.

        Returns:
            the established connection object
        """
        if self.connection is not None:
            return self.connection

        self.connection = Minio(f'{self.host}:{self.port}',
                                access_key=self.access_key,
                                secret_key=self.secret_key,
                                secure=self.secure)
        self.access_key = self.secret_key = None
        return self.connection

    @property
    def is_connected(self) -> bool:
        """Checks whether a connection to the S storage backend was established or not.

        Returns:
            True if storage connection exists, else False
        """
        return self.connection is not None

    def list_buckets(self) -> List[Bucket]:
        """List all existing buckets.

        Returns:
            a list of all existing buckets.
        """
        return self.connection.list_buckets()

    def create_bucket(self, bucket_name, location='eu-central-1'):
        """Create a new bucket.

        Args:
            bucket_name: the name of the bucket.
            location: the region where the bucket is to be created (default: eu-central-1)
                full list at https://docs.min.io/docs/python-client-api-reference#make_bucket
        """
        self.connection.make_bucket(bucket_name, location=location)

    def delete_bucket(self, bucket_name: str, force: bool = False) -> None:
        """Deletes a specific bucket.

        Args:
            bucket_name: the name of the bucket.
            force: has no effect with this backend
        """
        try:
            self.connection.remove_bucket(bucket_name)
        except ResponseError as err:
            print(err)

    def bucket_exists(self, bucket_name: str) -> bool:
        """Checks if a bucket exists.

        Args:
            bucket_name: the name of the bucket.

        Returns:
            True if the bucket exists, else False
        """
        try:
            return self.connection.bucket_exists(bucket_name)
        except ResponseError as err:
            print(err)
            return False

    def incomplete_uploads(self, bucket_name: str, prefix: str, recursive: bool = False):
        """List all incomplete uploads for a given bucket.

        Examples:
            incomplete_uploads = minio.list_incomplete_uploads('foo')
            for current_upload in incomplete_uploads:
                print(current_upload)
            # hello
            # hello/
            # hello/
            # world/

            incomplete_uploads = minio.list_incomplete_uploads('foo', prefix='hello/')
            for current_upload in incomplete_uploads:
                print(current_upload)
            # hello/world/

            incomplete_uploads = minio.list_incomplete_uploads('foo', recursive=True)
            for current_upload in incomplete_uploads:
                print(current_upload)
            # hello/world/1
            # world/world/2
            # ...

            incomplete = minio.list_incomplete_uploads('foo', prefix='hello/', recursive=True)
            for current_upload in incomplete:
                print(current_upload)
            # hello/world/1
            # hello/world/2

        Args:
            bucket_name: Bucket to list incomplete uploads
            prefix: String specifying objects returned must begin with.
            recursive: If yes, returns all incomplete uploads for
                a specified prefix.

        Returns:
            a generator of incomplete uploads in alphabetical order.
        """
        return self.connection.list_incomplete_uploads(bucket_name=bucket_name,
                                                       prefix=prefix,
                                                       recursive=recursive)

    def remove_incomplete(self, bucket_name: str, object_name: str) -> None:
        """Remove all incomplete uploads for a given bucket_name and object_name.

        Args:
            bucket_name: Bucket to drop incomplete uploads
            object_name: Name of object to remove incomplete uploads
        """
        try:
            self.connection.remove_incomplete_upload(bucket_name=bucket_name,
                                                     object_name=object_name)
        except ResponseError as err:
            print(err)

    def put(self,
            uri: Union[str, ArtifactLocation],
            data: Union[bytes, io.IOBase, str, Path],
            content_type: str = 'application/octet-stream',
            metadata: dict = None,
            sse: dict = None,
            progress=None,
            part_size: int = (5 * 1024 * 1024)):
        """Add a new object to S3 storage.

        NOTE: Maximum object size supported by this API is 5TiB.

        Examples:
         # For adding an object from IOBase
         file_stat = os.stat('hello.txt')
         with open('hello.txt', 'rb') as data:
             minio.put_object('foo', 'bar', data, file_stat.st_size, 'text/plain')

        # For adding from a file
        minio.fput_object('foo', 'bar', 'filepath', 'text/plain')


        - For length lesser than 5MB put_object automatically
          does single Put operation.
        - For length larger than 5MB put_object automatically
          does resumable multipart operation.

        Args:
            uri: the uri under which the object should be stored
            data: the contents to upload.
            content_type: mime type of object as a string.
            metadata: Any additional metadata to be uploaded along
                with your PUT request.
            sse: Server-side encryption.
            progress: A progress object (optional)
            part_size: Multipart part size (if multipart upload is done)

        Returns:
            etag
        """
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        # check if the bucket exits and create it if it does not exist
        if not self.bucket_exists(uri.bucket):
            self.create_bucket(uri.bucket)

        # if it is a bytes object wrap it with BytesIO reader
        if isinstance(data, bytes):
            data = io.BytesIO(data)

        if issubclass(type(data), (io.IOBase, bytes)):
            length = compute_stream_length(data)

            # tags = Tags(for_object=True)
            # tags["User"] = "jsmith"
            # tags["Type"] = "Model"

            return self.connection.put_object(bucket_name=uri.bucket,
                                              object_name=uri.artifact_path,
                                              data=data,
                                              length=length,
                                              content_type=content_type,
                                              metadata=metadata,
                                              sse=sse,
                                              progress=progress,
                                              part_size=part_size)
        if isinstance(data, (str, Path)):

            if isinstance(data, Path):
                data = str(data)

            return self.connection.fput_object(bucket_name=uri.bucket,
                                               object_name=uri.artifact_path,
                                               file_path=data,
                                               content_type=content_type,
                                               metadata=metadata,
                                               sse=sse,
                                               progress=progress,
                                               part_size=part_size)
        raise ValueError(f"An error occurred during processing of data={type(data)}")

    def get(
            self,
            uri: Union[str, ArtifactLocation],
            offset: int = None,
            length: int = None,
            # file_path: str = None,
            request_headers: dict = None,
            ssec: bytes = None) -> bytes:
        """Retrieves an object from a bucket.

        Retrieves an object from a bucket and either writes it to the specified file path or
        returns it as an open network connection (HTTPResponse) to enable incremental
        consumption of the response. To re-use the connection (if desired) on subsequent
        requests, the user needs to call `release_conn()` on the
        returned object after processing.

        Examples:
            # write to file
            minio.fget_object('foo', 'bar', 'localfile')

            # get object as HTTPResponse
            my_object = minio.get_partial_object('foo', 'bar')

        Args:
            uri: the uri to read object from.
            offset: Start byte position of object data. Default is beginning of the object data.
            length: Number of bytes of object data from offset. Default is till end of object data.
            file_path: Local file path to save the object (Optional).
            request_headers: Any additional headers to be added with GET request.
            ssec: Server-side encryption customer key.

        Returns:
            an open network connection (HTTPResponse) to read from or None if written to file.
        """
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        if ssec is not None:
            ssec = SseCustomerKey(ssec)

        blob = self.connection.get_object(bucket_name=uri.bucket,
                                          object_name=uri.artifact_path,
                                          request_headers=request_headers,
                                          ssec=ssec)
        return blob.read()

    @dispatch
    def delete(self, uri: str) -> None:
        """Deletes an artifact object from the storage.

        Args:
            uri: the uri of the artifact to be deleted.
        """
        self.delete(ArtifactLocation.from_uri(uri))

    @dispatch
    def delete(self, uri: ArtifactLocation) -> None:  # noqa: F811
        """Deletes an artifact object from the storage.

        Args:
            uri: the uri of the artifact to be deleted.
        """
        return self.connection.remove_object(uri.bucket, uri.artifact_path)

    @dispatch
    def delete(self, uris: List[str]) -> None:  # noqa: F811
        """Deletes multiple artifact objects from the storage.

        Args:
            uris: a list of artifact uris to be deleted.
        """
        self.delete([ArtifactLocation.from_uri(uri) for uri in uris])

    @dispatch
    def delete(self, uris: List[ArtifactLocation]) -> None:  # noqa: F811
        """Deletes multiple artifact objects from the storage.

        Args:
            uris: a list of artifact uris to be deleted.
        """
        buckets = defaultdict(list)
        for uri in uris:
            buckets[uri.bucket].append(DeleteObject(uri.artifact_path))

        for bucket, artifacts in buckets.items():
            errors = self.connection.remove_objects(bucket_name=bucket,
                                                    delete_object_list=artifacts)
            for error in errors:
                print("Error(s) occurred during deleting object", error)

    def list(self, uri: Union[str, ArtifactLocation]) -> Iterable:
        """Lists objects stored in a bucket.

        Args:
            uri: the name of the bucket

        Returns:
            an iterator for all the objects in the bucket.
        """
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        if uri.path is None:
            return self.connection.list_objects(bucket_name=uri.bucket)

        return self.connection.list_objects(bucket_name=uri.bucket, prefix=f'{uri.path}/')

    def _stat(self, uri: Union[str, ArtifactLocation], ssec: bytes = None):
        """Gets metadata of an object.

        If provided metadata key is not one of the valid/supported metadata names when
        the object was put/fput, the metadata information is saved with prefix X-Amz-Meta-
        prepended to the original metadata key name. So, the metadata returned by
        stat_object api will be presented with the original metadata key name prepended
        with X-Amz-Meta-.

        Args:
            uri: the uri of the object
            ssec: Server-Side Encryption headers (optional, defaults to None).

        Returns:
            object containing stat info
            (https://docs.min.io/docs/python-client-api-reference.html#stat_object)
            or None if the object does not exist.
        """
        if ssec is not None:
            ssec = SseCustomerKey(ssec)

        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        try:
            return self.connection.stat_object(bucket_name=uri.bucket,
                                               object_name=uri.artifact_path,
                                               ssec=ssec)
        except ResponseError as err:
            print(err)
            return None

    def __contains__(self, uri: Union[str, ArtifactLocation]) -> bool:
        """Checks if the specified object exists in the specified bucket.

        Args:
            uri: the uri of the object

        Returns:
            True if the specified object exists in the specified bucket else False
        """
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        try:
            if self._stat(uri):
                return True
        except S3Error:
            pass
        return False

    def metadata(self, uri: Union[str, ArtifactLocation], ssec: bytes = None) -> dict:
        """Retrieve the metadata of a stored object.

        Args:
            uri: the uri of the object
            ssec: Server-Side Encryption headers (optional, defaults to None).

        Returns:
            the metadata of a stored object.
        """
        stat = self._stat(uri, ssec=ssec)
        metadata = dict(stat.metadata)
        system_metadata = {
            key.lower(): value
            for key, value in metadata.items() if not key.startswith('x-amz-meta-')
        }
        user_metadata = {
            key.lstrip('x-amz-meta-'): value
            for key, value in metadata.items() if key.startswith('x-amz-meta-')
        }
        return system_metadata | user_metadata
