# -*- coding: utf-8 -*-
"""The storage module defines an interface as well as implementations of object storage backends."""
from .storage import Storage  # noreorder
from .manager import StorageManager

__all__ = ["Storage", "StorageManager"]
