# -*- coding: utf-8 -*-
"""This module contains shared functions for all backends."""


def compute_stream_length(data):
    """Returns the total length / size of an object/data.

    Args:
        data: the data whose size is to be computed

    Returns:
        the size / length of the data
    """
    if isinstance(data, bytes):
        return len(data)

    end = data.seek(0, 2)  # get the end of the stream
    data.seek(0)
    return end
