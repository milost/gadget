# -*- coding: utf-8 -*-
"""This module contains the implementation of the Google Cloud Storage backend."""
import io
from collections import defaultdict
from pathlib import Path
from typing import Iterator
from typing import List
from typing import Optional
from typing import Union

import google.cloud.client
from google.cloud import storage as gcs
from google.cloud.storage.blob import Blob as GBlob
from google.cloud.storage.bucket import Bucket
from plum import dispatch

from gadget.exceptions import ArtifactNotFound
from gadget.exceptions import BucketNotFound
from gadget.models import ArtifactLocation
from gadget.storage import Storage

# pylint: disable=too-many-arguments,function-redefined,redefined-argument-from-local,arguments-renamed


class GCStorage(Storage):
    """This class implements the Google Cloud object storage backend.

    In order to connect to GCS it is required that the
    GOOGLE_APPLICATION_CREDENTIALS environment variable is set.
    see: https://cloud.google.com/docs/authentication/getting-started

    Examples:
    .. code-block:: python

        # Connect to Google Cloud Storage
        storage = GCStorage()
        storage.connect()
    """
    def __init__(self):
        """Constructor of the Google Cloud storage backend."""
        self.client = None

    def connect(self) -> None:
        """Establish a connection to Google Cloud Storage.

        In order to connect to GCS it is required that the
        GOOGLE_APPLICATION_CREDENTIALS environment variable is set.
        see: https://cloud.google.com/docs/authentication/getting-started
        """
        self.client: google.cloud.client.Client = gcs.Client()

    def disconnect(self) -> None:
        """Disconnect from Google Cloud Storage."""
        self.client.close()
        self.client = None

    @property
    def is_connected(self) -> bool:
        """Checks whether a connection to Google Cloud Storage was established or not.

        Returns:
            True if storage connection is established, else False
        """
        return self.client is not None

    def list_buckets(self) -> List[Bucket]:
        """List all existing buckets.

        Returns:
            a list of all existing buckets.
        """
        return self.client.list_buckets()

    def create_bucket(self, bucket_name: str, location: str = 'us-east1') -> Bucket:
        """Create a new bucket.

        Examples:
        .. code-block:: python

            # Create bucket
            storage.create_bucket('my-little-bucket', location='us-east1')

            # Create bucket with location
            storage.create_bucket('my-little-bucket')

        Args:
            bucket_name: the name of the bucket.
            location: the region where the bucket is to be created. Defaults to us-east1
        """
        return self.client.create_bucket(bucket_or_name=bucket_name, location=location)

    def delete_bucket(self, bucket_name: str, force: bool = False) -> None:
        """Deletes a specific bucket.

        Args:
            bucket_name: the name of the bucket.
            force: if True, empties the bucket’s objects then deletes it.
        """
        bucket = self.lookup_bucket(bucket_name)
        if bucket is not None:
            bucket.delete(force=force)

    def lookup_bucket(self, bucket_name: str) -> Optional[Bucket]:
        """Retrieves a bucket if it exists.

        Args:
            bucket_name: the bucket to retrieve

        Returns:
            a bucket object or None if it did not exist.
        """
        return self.client.lookup_bucket(bucket_name)

    def bucket_exists(self, bucket_name: str) -> bool:
        """Checks if a bucket exists.

        Args:
            bucket_name: the name of the bucket.

        Returns:
            True if the bucket exists, else False
        """
        bucket = self.lookup_bucket(bucket_name)
        return bucket is not None

    def list(self, uri: Union[str, ArtifactLocation]) -> Iterator:
        """Lists objects stored in a bucket.

        Args:
            uri: uri whose artifact objects are to be listed

        Returns:
            an iterator for all the artifact objects in the bucket.
        """
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        bucket = self.lookup_bucket(uri.bucket)
        if bucket is not None:
            if uri.path is None:
                return bucket.list_blobs()
            return bucket.list_blobs(prefix=uri.path)
        raise BucketNotFound(f'Bucket: "{uri.bucket}" does not exist!')

    def put(self,
            uri: Union[str, ArtifactLocation],
            data: Union[bytes, io.IOBase, str, Path],
            content_type: str = 'application/octet-stream',
            metadata: dict = None,
            sse: dict = None,
            progress=None,
            part_size: int = None) -> None:
        """Add/Upload and artifact to Google Cloud Storage.

        Args:
            uri: the uri under which the object should be stored
            data: the contents to upload.
            content_type: mime type of object as a string.
                Defaults to application/octet-stream
            metadata: Any additional metadata to be uploaded along
                with your PUT request.
            sse: Server-side encryption.
            progress: A progress object (optional)
            part_size: Multipart part size (if multipart upload is done)
        """
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        bucket = self.lookup_bucket(uri.bucket)
        if bucket is None:
            bucket = self.create_bucket(uri.bucket)

        if bucket is not None:
            blob = GBlob(uri.artifact_path, bucket)
            blob.metadata = metadata
            if isinstance(data, (bytes, io.IOBase)):
                if isinstance(data, bytes):
                    data = io.BytesIO(data)
                blob.upload_from_file(data, content_type=content_type)
            elif isinstance(data, str):
                pass  # interpret string as path to file
        else:
            raise BucketNotFound(f'Bucket: "{uri.bucket}" does not exist!')

    def get(self,
            uri: Union[str, ArtifactLocation],
            offset: int = None,
            length: int = None,
            request_headers: dict = None,
            ssec: dict = None):
        """Retrieves the content of an object.

        Args:
            uri: the uri of the object.
            offset: Start byte position of object data. Default is beginning of the object data.
            length: Number of bytes of object data from offset. Default is till end of object data.
            request_headers: Any additional headers to be added with GET request.
            ssec: Server-side encryption customer key.

        Returns:
            an artifact object
        """
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        bucket = self.lookup_bucket(uri.bucket)
        if bucket is not None:
            blob = bucket.get_blob(uri.artifact_path)
            if blob is None:
                raise ArtifactNotFound(f'Artifact: "{uri.artifact_name}" does not exist!')
            return blob.download_as_bytes()
        raise BucketNotFound(f'Bucket: "{uri.bucket}" does not exist!')

    @dispatch
    def delete(self, uri: str) -> None:
        """Deletes an artifact object from the storage.

        Args:
            uri: the uri of the artifact to be deleted.
        """
        uri = ArtifactLocation.from_uri(uri)
        self.delete(uri)

    @dispatch
    def delete(self, uri: ArtifactLocation):  # noqa: F811
        """Deletes an artifact object from the storage.

        Args:
            uri: the uri of the artifact to be deleted.
        """
        bucket = self.lookup_bucket(uri.bucket)
        if bucket is not None:
            bucket.delete_blob(uri.artifact_path)
        else:
            raise BucketNotFound(f'Bucket: "{uri.bucket}" does not exist!')

    @dispatch
    def delete(self, uris: List[str]):  # noqa: F811
        """Deletes multiple artifact objects from the storage.

        Args:
            uris: a list of artifact uris to be deleted.
        """
        uris = [ArtifactLocation.from_uri(uri) for uri in uris]
        self.delete(uris)

    @dispatch
    def delete(self, uris: List[ArtifactLocation]):  # noqa: F811
        """Deletes multiple artifact objects from the storage.

        Args:
            uris: a list of artifact uris to be deleted.
        """
        buckets = defaultdict(list)
        for uri in uris:
            buckets[uri.bucket].append(uri)

        for bucket_name, uris in buckets.items():
            bucket = self.lookup_bucket(bucket_name)
            if bucket is not None:
                bucket.delete_blobs([uri.artifact_path for uri in uris])
            else:
                raise BucketNotFound(f'Bucket: "{bucket_name}" does not exist!')

    def __contains__(self, uri: Union[str, ArtifactLocation]) -> bool:
        """Checks if the specified object exists.

        Args:
            uri: the uri of the object

        Returns:
            True if the specified object exists, else False
        """
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        bucket = self.lookup_bucket(uri.bucket)
        if bucket is not None:
            blob = bucket.get_blob(uri.artifact_path)
            return blob is not None
        raise BucketNotFound(f'Bucket: "{uri.bucket}" does not exist!')

    def metadata(self, uri: Union[str, ArtifactLocation], ssec: dict = None) -> Optional[dict]:
        """Retrieve the metadata of a stored object.

        Args:
            uri: the uri of the object
            ssec: Server-Side Encryption headers (optional, defaults to None).

        Returns:
            the metadata of the stored object.
        """
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        bucket = self.lookup_bucket(uri.bucket)
        if bucket is not None:
            blob = bucket.get_blob(uri.artifact_path)
            if blob is None:
                raise ArtifactNotFound(f'Artifact: "{uri.artifact_name}" does not exist!')
            return blob.metadata
        raise BucketNotFound(f'Bucket: "{uri.bucket}" does not exist!')
