# -*- coding: utf-8 -*-
"""This module contains the storage backend manager.

Examples:
    .. code-block:: python

        storage_factory = functools.partial(S3Storage,
                                            host='localhost',
                                            port=9000,
                                            access_key='username',
                                            secret_key='password',
                                            secure=False)
        StorageManager.register('s3', storage_factory, True)
"""
from typing import Dict
from typing import Optional

from gadget.storage import Storage

_factories = {}
KB = 1024


class StorageManager:
    """Manages all registered store backends."""
    _default_storage_backend: str = None
    """Contains the storage backend that is currently set as the default backend."""
    _stores: Dict[str, Storage] = {}
    """Contains all registered storage backends"""
    @property
    def stores(self) -> Dict[str, Storage]:
        """Returns all registered storage backends.

        Returns:
            all registered storage backends.
        """
        if self._stores is None:
            self._stores = {}
        return self._stores

    @property
    def default_backend(self) -> Storage:
        """Return the currently set default storage backend."""
        return self.get()

    @classmethod
    def set_default_backend(cls, storage_id: str) -> None:
        """Set default storage backend.

        Sets a registered storage backend to be the default storage backend.
        Raises KeyError if no storage is registered with the specified storage_id.

        Args:
            storage_id: the id of the storage backend that is be set as default backend
        """
        if storage_id in cls._stores:
            cls._default_storage_backend = storage_id
        else:
            raise KeyError(f'No storage with storage id "{storage_id}" was found')

    @classmethod
    def register(cls, storage_id: str, storage_backend, default: bool = False) -> None:
        """Register a storage backend with the backend manager.

        Args:
            storage_id: a unique id under which the storage backend is to be registered.
            storage_backend: the storage backend to be registered.
            default: True if the backend should be the default backend, else False
        """
        _factories[storage_id] = storage_backend
        if default:
            cls._default_storage_backend = storage_id

    @classmethod
    def unregister(cls, storage_id: str) -> Storage:
        """Removes a storage backend from the backend manager.

        Raises KeyError if no backend storage with storage_id
        is present.

        Args:
            storage_id: the id of the storage backend that is
                to be removed from the registry.

        Returns:
            the storage that was removed
        """
        if storage_id == cls._default_storage_backend:
            cls._default = None

        if storage_id in _factories:
            return _factories.pop(storage_id)

        raise KeyError(f'No storage with id: {storage_id}')

    @classmethod
    def get(cls, storage_id: Optional[str] = None) -> Storage:
        """Lookup the store in registered storage backends.

        Lookup the store in available instance cache, and instantiate a new
        one using registered factory function, if not found.
        If the key is :data:`.None`, the default store will be
        instantiated(if required) and returned.

        Args:
            storage_id: the store unique id to lookup.
        """
        if storage_id is None:
            if cls._default_storage_backend is None:
                raise ValueError('There is no default storage backend set')
            storage_id = cls._default_storage_backend

        if storage_id not in cls._stores:
            factory = _factories[storage_id]
            cls._stores[storage_id] = factory()  # create instance of storage backend
            if not cls._stores[storage_id].is_connected:
                cls._stores[storage_id].connect()
        return cls._stores[storage_id]
