# -*- coding: utf-8 -*-
"""This module contains an abstract storage interface.

The defined methods must be implemented by all storage backends.
"""
import io
from abc import ABC
from abc import abstractmethod
from pathlib import Path
from typing import Iterator
from typing import List
from typing import Union

# pylint: disable=too-many-arguments


class Storage(ABC):
    """An interface that needs to be implemented by concrete storage backends."""
    @abstractmethod
    def connect(self):
        """Establish a connection to the storage backend.

        Returns:
             the established connection object
        """
        raise NotImplementedError

    @property
    @abstractmethod
    def is_connected(self):
        """Checks whether a connection to the storage backend was established or not.

        Returns:
            bool: True if storage connection is established, else False
        """
        raise NotImplementedError

    @abstractmethod
    def list_buckets(self):
        """List all existing buckets.

        Returns:
            a list of all existing buckets.
        """
        raise NotImplementedError

    @abstractmethod
    def create_bucket(self, bucket_name: str, location: str) -> None:
        """Create a new bucket.

        For some backends a region might not be applicable.

        Args:
            bucket_name: the name of the bucket.
            location: the region in which the bucket will be created.
                full list at https://docs.min.io/docs/python-client-api-reference#make_bucket
        """
        raise NotImplementedError

    @abstractmethod
    def delete_bucket(self, bucket_name: str, force: bool = False) -> None:
        """Deletes a specific bucket.

        Args:
            bucket_name: the name of the bucket to be deleted.
            force: if True, the deletion of the bucket will be enforced
        """
        raise NotImplementedError

    @abstractmethod
    def bucket_exists(self, bucket_name: str) -> bool:
        """Checks if a bucket with a specific name exists.

        Args:
            bucket_name: the name of the bucket whose existence is to be checked.

        Returns:
            True if the bucket exists, else False
        """
        raise NotImplementedError

    @abstractmethod
    def list(self, uri: Union[str, 'ArtifactLocation']) -> Iterator:  # noqa: F821
        """Lists objects stored in a bucket.

        Args:
            uri: uri whose artifact objects are to be listed

        Returns:
            an iterator for all the artifact objects in the bucket.
        """
        raise NotImplementedError

    @abstractmethod
    def put(
            self,
            uri: Union[str, 'ArtifactLocation'],  # noqa: F821
            data: Union[bytes, io.IOBase, str, Path],
            content_type: str = 'application/octet-stream',
            metadata: dict = None,
            sse: dict = None,
            progress=None,
            part_size: int = None):
        """Add a new object to the storage.

        Examples:
            Some examples need to go here...

        Args:
            uri: the uri where the object should be stored
                uri format is: [bucket_name]/[path]/[filename].[extension]
            data: an object having callable read() returning a bytes object.
            content_type: the content type of object.
            metadata: Any additional metadata to be uploaded along
                with your data.
            sse: Server-side encryption.
            progress: A progress object (optional)
            part_size: Multipart part size (in case of multipart upload)

        Returns:
            an object that contains information about the write result.
        """
        raise NotImplementedError

    @abstractmethod
    def get(
            self,
            uri: Union[str, 'ArtifactLocation'],  # noqa: F821
            offset: int = None,
            length: int = None,
            request_headers: dict = None,
            ssec: dict = None):
        """Retrieves an object from storage.

        Retrieves an object from a bucket and either writes it to the specified file_path or
        returns it as an open network connection (HTTPResponse) to enable incremental
        consumption of the response.

        Examples:
            Some examples need to go here ...

        Args:
            uri: the uri to read object from.
                uri format is: [bucket_name]/[path]/[filename].[extension]
            offset: Start byte position of object data. Default is beginning of the object data.
            length: Number of bytes of object data from offset. Default is till end of object data.
            request_headers: Any additional headers to be added with GET request.
            ssec: Server-side encryption customer key.

        Returns:
            an open network connection (HTTPResponse) to read from or None if written to file.
            The returned response should be closed after use to release network resources.
        """
        raise NotImplementedError

    @abstractmethod
    def delete(self, uris: Union[str, List[str]]):
        """Deletes either one or multiple objects from the storage.

        Args:
            uris: either the uri of the object to be deleted or a list of uris to be deleted.
                uri format is: [bucket_name]/[path]/[filename].[extension]

        Returns:
            if several objects are deleted returns one error report per object
        """
        raise NotImplementedError

    @abstractmethod
    def __contains__(self, uri: Union[str, 'ArtifactLocation']):  # noqa: F821
        """Checks whether an object exists under the specified URI.

        Args:
            uri: the uri of the object to be checked.
                uri format is: [bucket_name]/[path]/[filename].[extension]

        Returns:
            True if an object exists under the specified URI else False
        """
        raise NotImplementedError

    def metadata(self, uri: Union[str, 'ArtifactLocation'], ssec: dict = None):  # noqa: F821
        """Retrieve the metadata of a stored object.

        Args:
            uri: the uri of the object
                uri format is: [bucket_name]/[path]/[filename].[extension]
            ssec: Server-Side Encryption headers (optional, defaults to None).

        Returns:
            the metadata stored alongside the object.
        """
        raise NotImplementedError
