# -*- coding: utf-8 -*-
"""This module contains the implementation of a S3 storage backend."""
import io
import os
import shutil
from pathlib import Path
from typing import Iterable
from typing import List
from typing import Tuple
from typing import Union

import urllib3
from minio import Minio
from minio.datatypes import Bucket
from minio.deleteobjects import DeleteError

from gadget.models import ArtifactLocation
from gadget.storage.storage import Storage

# from minio.commonconfig import Tags

# pylint: disable=too-many-arguments, duplicate-code, unused-argument


class FileStorage(Storage):
    """This class implements the dummy backend that just uses the local file system.

    Examples:
            # Uses cwd
            storage = FileStorage()

            # Use some other directory
            storage = FileStorage('../some/base/dir')

            storage.connect()  # does nothing in this case
    """
    def __init__(
        self,
        base_dir: str = None,
    ) -> None:
        """Construct local dir.

        Args:
            base_dir: if provided, use as the root dir for the system.
                Default is the current working directory.
        """
        self.base_dir = os.path.abspath(base_dir or os.getcwd())

    def connect(self) -> Minio:
        """No op."""

    @property
    def is_connected(self) -> bool:
        """File system is connected by definition."""
        return True

    def _compute_stream_length(self, data):
        """Return the total length / size of an object/data.

        Args:
            data: the data whose size is to be computed

        Returns:
            the size / length of the data
        """
        if isinstance(data, bytes):
            return len(data)

        end = data.seek(0, 2)  # get the end of the stream
        data.seek(0)
        return end

    def _split_storage_path(self, storage_uri: str) -> Tuple[str, str]:
        """Separate the storage uri into bucket_name and object_name.

        Args:
            storage_uri: the complete storage uri

        Returns:
            a tuple consisting of (bucket_name, object_name)
        """
        segments = storage_uri.split("/")

        bucket_name = segments[0]
        uri = "/".join(segments[1:])

        return bucket_name, uri

    def list_buckets(self) -> List[Bucket]:
        """List all existing buckets. This just lists the dir, we do not enforce actual bucketing.

        Returns:
            a list of all existing buckets.
        """
        return os.listdir(self.base_dir)

    def create_bucket(self, bucket_name, location="eu-central-1"):
        """Create a new bucket.

        Args:
            bucket_name: the name of the bucket.
            location: the region where the bucket is to be created (default: eu-central-1)
                full list at https://docs.min.io/docs/python-client-api-reference#make_bucket
        """
        self.connection.make_bucket(bucket_name, location=location)
        path = os.path.join(self.base_dir, bucket_name)
        os.makedirs(path)

    def delete_bucket(self, bucket_name: str, force: bool = False) -> None:
        """Deletes a specific bucket.

        Args:
            bucket_name: the name of the bucket.
            force: has no effect on this backend.
        """
        try:
            path = os.path.join(self.base_dir, bucket_name)
            shutil.rmtree(path)
        except Exception as err:  # pylint: disable=broad-except
            print(err)

    def bucket_exists(self, bucket_name: str) -> bool:
        """Checks if a bucket exists.

        Args:
            bucket_name: the name of the bucket.

        Returns:
            True if the bucket exists, else False
        """
        path = os.path.join(self.base_dir, bucket_name)
        return os.path.exists(path)

    def incomplete_uploads(self, bucket_name: str, prefix: str, recursive: bool = False):
        """List all incomplete uploads for a given bucket.

        Examples:
            incomplete_uploads = minio.list_incomplete_uploads('foo')
            for current_upload in incomplete_uploads:
                print(current_upload)
            # hello
            # hello/
            # hello/
            # world/

            incomplete_uploads = minio.list_incomplete_uploads('foo', prefix='hello/')
            for current_upload in incomplete_uploads:
                print(current_upload)
            # hello/world/

            incomplete_uploads = minio.list_incomplete_uploads('foo', recursive=True)
            for current_upload in incomplete_uploads:
                print(current_upload)
            # hello/world/1
            # world/world/2
            # ...

            incomplete = minio.list_incomplete_uploads('foo', prefix='hello/', recursive=True)
            for current_upload in incomplete:
                print(current_upload)
            # hello/world/1
            # hello/world/2

        Args:
            bucket_name: Bucket to list incomplete uploads
            prefix: String specifying objects returned must begin with.
            recursive: If yes, returns all incomplete uploads for
                a specified prefix.

        Returns:
            a generator of incomplete uploads in alphabetical order.
        """
        return []

    def remove_incomplete(self, bucket_name: str, object_name: str) -> None:
        """Remove all incomplete uploads for a given bucket_name and object_name.

        Args:
            bucket_name: Bucket to drop incomplete uploads
            object_name: Name of object to remove incomplete uploads
        """

    def put(
            self,
            uri: ArtifactLocation,
            data: Union[bytes, io.IOBase, str, Path],
            content_type: str = "application/octet-stream",
            metadata: dict = None,
            sse: dict = None,
            progress=None,
            part_size: int = (5 * 1024 * 1024),
    ):
        """Add a new object to S3 storage.

        NOTE: Maximum object size supported by this API is 5TiB.

        Examples:
         # For adding an object from IOBase
         file_stat = os.stat('hello.txt')
         with open('hello.txt', 'rb') as data:
             minio.put_object('foo', 'bar', data, file_stat.st_size, 'text/plain')

        # For adding from a file
        minio.fput_object('foo', 'bar', 'filepath', 'text/plain')


        - For length lesser than 5MB put_object automatically
          does single Put operation.
        - For length larger than 5MB put_object automatically
          does resumable multipart operation.

        Args:
            uri: the ArtifactLocation under which the object should be stored
            data: the contents to upload.
            content_type: mime type of object as a string.
            metadata: Any additional metadata to be uploaded along
                with your PUT request.
            sse: Server-side encryption.
            progress: A progress object (optional)
            part_size: Multipart part size (if multipart upload is done)

        Returns:
            etag
        """
        path = os.path.join(self.base_dir, uri.uri)
        parent_path = os.path.dirname(path)
        # bucket_name, object_name = self._split_storage_path(path)

        # check if the bucket exits and create it if it does not exist
        if not os.path.exists(parent_path):
            os.makedirs(parent_path)

        if issubclass(type(data), (io.IOBase, bytes)):
            # Write bytes to the file
            with open(path, "wb") as f:
                f.write(data)
        else:
            # Just copy the file
            shutil.copy(data, path)

    def get(
        self,
        uri: ArtifactLocation,
        offset: int = None,
        length: int = None,
        # file_path: str = None,
        request_headers: dict = None,
        ssec: bytes = None,
    ) -> Union[urllib3.response.HTTPResponse, dict]:
        """Retrieves an object from a bucket.  In this case it's just the open file object.

        Retrieves an object from a bucket and either writes it to the specified file path or
        returns it as an open network connection (HTTPResponse) to enable incremental
        consumption of the response. To re-use the connection (if desired) on subsequent
        requests, the user needs to call `release_conn()` on the
        returned object after processing.

        Examples:
            # write to file
            minio.fget_object('foo', 'bar', 'localfile')

            # get object as HTTPResponse
            my_object = minio.get_partial_object('foo', 'bar')

        Args:
            uri: the ArtifactLocation to read object from.
            offset: Start byte position of object data. Default is beginning of the object data.
            length: Number of bytes of object data from offset. Default is till end of object data.
            file_path: Local file path to save the object (Optional).
            request_headers: Any additional headers to be added with GET request.
            ssec: Server-side encryption customer key.

        Returns:
            an open network connection (HTTPResponse) to read from or None if written to file.
        """
        path = os.path.join(self.base_dir, uri.uri)
        return open(path, "rb").read()

    def delete(self, uris: Union[ArtifactLocation,
                                 List[ArtifactLocation]]) -> Iterable[DeleteError]:
        """Deletes either one or multiple objects from the storage.

        Args:
            uris: either the uri of the object to be deleted or a list of uris to be deleted.

        Returns:
            if several objects are deleted returns one error report per object
        """
        if not isinstance(uris, list):
            uris = [uris]
        for uri in uris:
            path = os.path.join(self.base_dir, uri.uri)
            os.remove(path)

    def list(self, uri: Union[str, ArtifactLocation]) -> List[str]:
        """Lists objects stored in a bucket.

        Args:
            uri: the uri to the directory whose files are to be listed

        Returns:
            an iterator for all the objects in the bucket.
        """
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        path = os.path.join(self.base_dir, uri.bucket)
        fs = os.listdir(path)
        if uri.path is not None:
            return [f for f in fs if f.startswith(uri.path)]
        return fs

    def _stat(self, uri: str, ssec: bytes = None):
        """Gets metadata of an object.

        If provided metadata key is not one of the valid/supported metadata names when
        the object was put/fput, the metadata information is saved with prefix X-Amz-Meta-
        prepended to the original metadata key name. So, the metadata returned by
        stat_object api will be presented with the original metadata key name prepended
        with X-Amz-Meta-.

        Args:
            uri: the uri of the object
            ssec: Server-Side Encryption headers (optional, defaults to None).

        Returns:
            object containing stat info
            (https://docs.min.io/docs/python-client-api-reference.html#stat_object)
            or None if the object does not exist.
        """
        raise NotImplementedError("Not yet supported")

    def __contains__(self, uri: str) -> bool:
        """Checks if the specified object exists in the specified bucket.

        Args:
            uri: the uri of the object

        Returns:
            True if the specified object exists in the specified bucket else False
        """
        path = os.path.join(self.base_dir, uri)
        return os.path.exists(path)

    def metadata(self, uri: str, ssec: bytes = None) -> dict:
        """Retrieve the metadata of a stored object.

        Args:
            uri: the uri of the object
            ssec: Server-Side Encryption headers (optional, defaults to None).

        Returns:
            the metadata of a stored object.
        """
        raise NotImplementedError("Not yet supported")
