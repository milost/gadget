# -*- coding: utf-8 -*-
"""This module implements the data access object.

Examples:
    .. code-block:: python

        s3_storage = functools.partial(S3Storage,
                                       host='localhost',
                                       port=9000,
                                       access_key='[username]',
                                       secret_key='[password]',
                                       secure=False)

        gcs_storage = functools.partial(GCStorage)

        connection = Connection('localhost', 5432, '[username]', '[password]', '[database]')
        dao = DAO(connection)  # instantiate dao
        dao.register_storage('s3', s3_storage, default=True)
        dao.register_storage('gcs', gcs_storage, default=True)
        dao.init()  # setup connection etc.
"""
from __future__ import annotations

import logging
import sys
from dataclasses import dataclass

import sqlalchemy.orm
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy.orm import mapper
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import create_database
from sqlalchemy_utils import database_exists
from sqlalchemy_utils import force_auto_coercion

from gadget import Model
from gadget.storage import Storage
from gadget.storage import StorageManager


@dataclass
class Connection:
    """Stores connection parameters."""
    host: str
    port: int
    username: str
    password: str
    database: str

    def __str__(self):
        """Returns postgres connection string.

        Returns:
            connection string for connecting to postgres server
        """
        return f'postgresql://{self.username}:{self.password}@' \
               f'{self.host}:{self.port}/{self.database}'


class DAO:
    """Data access object."""
    def __init__(self,
                 connection: str | Connection,
                 log_level=logging.INFO,
                 log_stream=sys.stdout,
                 model_base_class=Model):
        """Constructor of data access object.

        Args:
            connection: the connection url or connection object to the relational database
            log_level: the log level with which SQLAlchemy logs (default is INFO)
            log_stream: the stream to which the log output is printed (default is stdout)
            model_base_class: the base class to be used for model creation.
            create_database_if_not_exists: if True and the specified database does not exist,
             it will be created
        """
        # Configure logging to stdout via python logging
        logging.basicConfig(stream=log_stream)
        logging.getLogger("sqlalchemy").setLevel(log_level)

        # create sqlalchemy engine
        if isinstance(connection, str):
            self.engine = create_engine(connection)
        elif isinstance(connection, Connection):
            self.engine = create_engine(
                f'postgresql://{connection.username}:{connection.password}@{connection.host}' +
                f':{connection.port}/{connection.database}')
        else:
            raise TypeError(f"Connection is of unknown type: {type(connection)}")

        # self.metadata = MetaData()
        self.Base = model_base_class

        # Refer https://docs.sqlalchemy.org/en/13/orm/contextual.html on how to use the Session.
        # We want to use a separate session for each request (i.e., call of an endpoint method
        # (GET, POST, etc.)).
        self.session_factory = sessionmaker(bind=self.engine)
        self.Session = scoped_session(self.session_factory)

        force_auto_coercion()

    @property
    def metadata(self) -> MetaData:
        """Get metadata.

        Returns:
            the metadata of this object
        """
        return self.Base.metadata

    def init(self, create_database_if_not_exists: bool = True):
        """Initializes the data access object."""
        if not database_exists(self.engine.url) and create_database_if_not_exists:
            create_database(self.engine.url)

        # Create tables etc.
        self.metadata.create_all(self.engine, checkfirst=True)

        # Pass the scoped session (the actual class) to the models.
        # This object will be uses to access the current session
        # (with the lifetime of a single request).
        self.Base.set_session(self.Session)

    def nuke_database(self) -> None:
        """Deletes all objects from database."""
        sqlalchemy.orm.close_all_sessions()
        self.metadata.drop_all(self.engine)

    def register_storage(self, storage_id: str, storage_factory, default: bool = False) -> None:
        """Register a storage backend.

        Args:
            storage_id: the storage id of the registered storage
            storage_factory: a factory capable of creating a storage
            default: if True, the storage will be set as default
        """
        StorageManager.register(storage_id, storage_factory, default)

    @property
    def storage(self) -> Storage:
        """Return default storage.

        Returns:
            default storage backend used for data persistence
        """
        return StorageManager.get()

    @property
    def session(self) -> session.Session:
        """Get session object.

        Returns:
            a session object
        """
        return self.Session()

    def add_mapping(self, cls, table, **kwargs):
        """To be documented."""
        mapper(cls, table, **kwargs)
