# -*- coding: utf-8 -*-
"""An easy to use ORM implementation based on SQL Alchemy."""
__author__ = """gadget"""
__email__ = "michael@loster.io"
__version__ = "0.3.2"

from gadget.mixins import ActiveRecordMixin

Model = ActiveRecordMixin
