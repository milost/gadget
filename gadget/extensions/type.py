# -*- coding: utf-8 -*-
"""This modules contains type extensions to SQLAlchemy."""
import re
import uuid
from typing import Any
from typing import Optional

import bcrypt
import sqlalchemy
from sqlalchemy import types
from sqlalchemy import util
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.dialects.postgresql.base import PGDialect as PostgreSQLDialect
from sqlalchemy.engine import Dialect

from gadget.exceptions import ListError

# pylint: disable=all


def version_to_int(version: str) -> int:
    """Turns version string into integer representation.

    Args:
        version: the version string that is to be converted

    Returns:
        an integer representing the version string
    """
    version_format = r"(\d+)\.(\d+)\.(\d+)-(\d+)"

    match = re.match(version_format, version)

    # for matchNum, match in enumerate(matches, start=1):
    print(f"Match was found at {match.start()}-{match.end()}: {match.group()}")
    return int(f'{match.group(1)}{match.group(2)}{match.group(3)}{match.group(4)}')


def int_to_version(version: int) -> str:
    """Converts integer into version string.

    Args:
        version: the integer that is to converted

    Returns:
        a version string
    """
    version = str(version)

    version_format = r"(\d)(\d)(\d)(\d+)"

    match = re.match(version_format, version)
    print(f"Match was found at {match.start()}-{match.end()}: {match.group()}")
    return f'{match.group(1)}.{match.group(2)}.{match.group(3)}-{match.group(4)}'


class Password(types.TypeDecorator):
    """The password type encrypts a password string."""

    impl = types.LargeBinary
    """Defines which type is to be extended"""
    @property
    def python_type(self):
        """Returns python type of the type implementation.

        Returns:
            python type of the type implementation.
        """
        return util.binary_type

    def process_bind_param(self, value, dialect: Dialect):
        """Encrypts a given value before it gets persisted to the database.

        Args:
            value: the value to be encrypted
            dialect: the dialect that is currently in use.

        Returns:
            an encrypted version of the passed value
        """
        return bcrypt.hashpw(value.encode('utf8'), bcrypt.gensalt())

    # set process_literal_param to use the process_bind_param function
    process_literal_param = process_bind_param

    def process_result_value(self, value, dialect: Dialect):
        """Returns the encrypted value.

        Args:
            value: the encrypted value
            dialect: the dialect that is currently in use.

        Returns:
            the encrypted value.
        """
        return value


class ListType(types.TypeDecorator):
    """Defines a List datatype.

    This datatype makes it possible to persist lists of values within a postgres column
    """

    impl = sqlalchemy.UnicodeText()
    """Defines which type is to be extended"""
    def __init__(self, coerce_func=str, separator='\t', *args, **kwargs):
        """Constructor of the List datatype.

        Args:
            coerce_func: to be documented
            separator: the separator to be used for separating individual list values.
            *args: additional positional arguments
            **kwargs: additional keyword arguments
        """
        super().__init__(*args, **kwargs)
        self.separator = separator
        self.coerce_func = coerce_func

    @property
    def python_type(self):
        """Returns python type of the type implementation.

        Returns:
            python type of the type implementation.
        """
        return list

    def process_bind_param(self, value: Optional[Any], dialect):
        """Converts a list of values into a unicode string.

        This method get the literal Python data value(s)
        which is to be associated with a bound parameter in
        the statement.

        Args:
            value: data to operate on, can be of any type expected
                by this method in the subclass. Can also be None.
            dialect: the dialect that is currently in use.

        Example:
             [1, 2, 3, 4] -> u'1, 2, 3, 4'

        Returns:
            a unicode string of a passed in list
        """
        if value is not None:
            # check that all list values are strings AND not the delimiter itself.
            if all(isinstance(item, str) for item in value):
                if any(self.separator in item for item in value):
                    raise ListError(f"List values can't contain string '{self.separator}'"
                                    "(its being used as separator. If you wish for scalar"
                                    "list values to contain these strings, use a different"
                                    "separator string.)")
            return self.separator.join(map(str, value))

    # set process_literal_param to use the process_bind_param function
    process_literal_param = process_bind_param

    def process_result_value(self, value, dialect):
        """Turn string representation of a list back into a python list object.

        Converts a row column value back to a python datatype.

        Example:
             u'1, 2, 3, 4' -> [1, 2, 3, 4]

        Args:
            value: a string representing a list of values
            dialect: the dialect that is currently in use.

        Returns:
            a python list object containing the individual values in the string.
        """
        if value is not None:
            if value == '':
                return []
            # coerce each value
            return list(map(self.coerce_func, value.split(self.separator)))


class GUID(types.TypeDecorator):
    """Platform-independent GUID type.

    Uses PostgreSQL's UUID type, otherwise uses
    CHAR(32), storing as stringified hex values.
    """
    impl = types.CHAR
    """Defines which type is to be extended"""

    cache_ok = True
    """indicates if this TypeDecorator is safe to be used as part of a cache key."""
    @property
    def python_type(self):
        """Returns python type of the type implementation.

        Returns:
            python type of the type implementation.
        """
        return uuid.UUID

    def load_dialect_impl(self, dialect: Dialect):
        """Get the type implementation depending on the dialect used.

        Args:
            dialect: the used dialect

        Returns:
            type implementation depending on the dialect
        """
        if isinstance(dialect, PostgreSQLDialect):
            return dialect.type_descriptor(UUID())
        else:
            return dialect.type_descriptor(types.CHAR(32))

    def process_bind_param(self, value, dialect: Dialect):
        """Converts UUID value to string representation.

        Args:
            value: a UUID
            dialect: the dialect that is currently in use.

        Returns:
            a string value of the UUID
        """
        if value is None:
            return value
        elif isinstance(dialect, PostgreSQLDialect):
            return str(value)
        else:
            if not isinstance(value, uuid.UUID):
                return "%.32x" % uuid.UUID(value).int
            else:
                # hexstring
                return "%.32x" % value.int

    # set process_literal_param to use the process_bind_param function
    process_literal_param = process_bind_param

    def process_result_value(self, value, dialect: Dialect):
        """Converts string value to UUID.

        Args:
            value: string that is to be converted to a UUID
            dialect: the dialect that is currently in use.

        Returns:
            a UUID
        """
        if value is None:
            return value
        else:
            if not isinstance(value, uuid.UUID):
                value = uuid.UUID(value)
            return value
