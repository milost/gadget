# -*- coding: utf-8 -*-
"""This module contains the serialization/deserialization mixin."""
import msgpack
from marshmallow import Schema

from gadget.utils import all_subclasses

# pylint: disable=C0415


class Serializable:
    """This mixin provides functionality to serialize and deserialize objects."""

    schema: Schema = None
    """the marshmallow schema that should be used for serialization and deserialization"""
    def dump(self) -> dict:
        """Serializes an object into a dictionary.

        Serialization of the object using the marshmallow schema defined by the object.

        Returns:
            a dictionary representation of the object
        """
        return self.schema.dump(self)

    def dumps(self) -> str:
        """Serializes an object into a string.

        Serialization of the object using the marshmallow schema defined by the object.

        Returns:
            a string representation of the object
        """
        return self.schema.dumps(self)

    @classmethod
    def load(cls, values) -> 'Serializable':
        """Deserialization of the given values into a corresponding object.

        Args:
            values: the values to be deserialized

        Returns:
            the deserialized object
        """
        return cls.__get_schema(values).load(values)

    @classmethod
    def loads(cls, values: str) -> 'Serializable':
        """Deserialization of the given string into a corresponding object.

        Args:
            the string to be deserialized into an object

        Returns:
            the deserialized object
        """
        return cls.__get_schema(values).loads(values)

    def to_bytes(self) -> bytes:
        """This method turns an object into a byte representation.

        Returns:
            the byte representation of the object
        """
        return msgpack.packb(self.dump(), use_bin_type=True)

    @classmethod
    def from_bytes(cls, data: bytes) -> 'Serializable':
        """Deserialize the byte representation of an object.

        Args:
            data:the byte representation of the object

        Returns:
            the deserialized object
        """
        return cls.schema.load(msgpack.unpackb(data, raw=False))

    @classmethod
    def __get_schema(cls, values):
        """Retrieves the schema that should be used for serialization/deserialization.

        For this purpose the field polymorphic_identity is used to
        access the corresponding schema.

        Args:
            values: the values to be deserialized to an object.

        Returns:
            the schema that should be used for deserialization
        """
        # turns values from string representation into a dictionary
        values_dict = values
        if isinstance(values_dict, str):
            import json
            values_dict = json.loads(values_dict)

        # use dictionary to find the polymorphic type of the object
        try:
            polymorphic_identity = values_dict[cls.polymorphic_field_id]
        except AttributeError:
            return cls.schema

        return cls.__get_subclass(polymorphic_identity).schema

    @classmethod
    def __get_subclass(cls, polymorphic_identity: str):
        """Uses the polymorphic_identity field to return the corresponding class.

        This way the class can be used for serialization and deserialization

        Args:
            polymorphic_identity: the polymorphic_identity of the class

        Returns:
            the class that matches the polymorphic_identity
        """
        for subclass in all_subclasses(cls):
            try:
                if subclass.__mapper_args__["polymorphic_identity"] == polymorphic_identity:
                    return subclass
            except (AttributeError, KeyError):
                continue
        return cls
