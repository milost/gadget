# -*- coding: utf-8 -*-
"""The SessionMixin makes it possible to attach an sqlalchemy session to a class."""
from typing import Type
from typing import Union

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Query
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import Session

from gadget.exceptions import NoSessionError
from gadget.utils import classproperty

# pylint: disable=E0213

Base = declarative_base()


class SessionMixin(Base):
    """The SessionMixin provides easy access to an sqlalchemy session object."""

    __abstract__ = True

    _Session = None
    """Stores the sqlalchemy session"""
    @classmethod
    def set_session(cls, session: Union[scoped_session, Type[Session]]):
        """Attach a session object to a class.

        Args:
            session: the session object that should be attached
        """
        cls._Session = session

    @classproperty
    def session(cls) -> Union[scoped_session, Type[Session]]:
        """Access the session object of a specific class.

        Returns:
            the session object of a class.
        """
        if cls._Session is not None:
            return cls._Session

        raise NoSessionError('Cant get session. Please, set_session() first')

    @classmethod
    def count(cls) -> int:
        """Get number of objects.

        Query the number of objects of a given class that exist in the database

        Returns:
            the number of objects of a given class that exist in the database
        """
        return cls.session.query(cls).count()

    @classmethod
    def commit(cls):
        """Commit all changes currently associated with this session."""
        cls.session.flush()
        cls.session.commit()

    @classproperty
    def query(cls) -> Query:
        """Get a query object.

        Returns:
            a query object
        """
        return cls.session.query(cls)
