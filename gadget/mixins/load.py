# -*- coding: utf-8 -*-
"""This module contains a mixin for eager loading."""
from typing import List
from typing import Union

from sqlalchemy.orm import joinedload
from sqlalchemy.orm import subqueryload
from sqlalchemy.orm.attributes import InstrumentedAttribute

from .session import SessionMixin

JOINED = 'joined'
SUBQUERY = 'subquery'


def eager_expr(schema: dict):
    """Creates an eager expression from a schema."""
    flat_schema = _flatten_schema(schema)
    return _eager_expr_from_flat_schema(flat_schema)


def _flatten_schema(schema: dict):
    """Flattens the passed in schema."""
    def _flatten(schema: dict, parent_path: str, result: dict):
        """Flattens a schema.

        Args:
            schema: to be documented
            parent_path: to be documented
            result: to be documented

        Returns:
            a flattened schema
        """
        for path, value in schema.items():
            # for supporting schemas like Product.user: {...},
            # we transform, say, Product.user to 'user' string
            if isinstance(path, InstrumentedAttribute):
                path = path.key

            if isinstance(value, tuple):
                join_method, inner_schema = value[0], value[1]
            elif isinstance(value, dict):
                join_method, inner_schema = JOINED, value
            else:
                join_method, inner_schema = value, None

            full_path = parent_path + '.' + path if parent_path else path
            result[full_path] = join_method

            if inner_schema:
                _flatten(inner_schema, full_path, result)

    result = {}
    _flatten(schema, '', result)
    return result


def _eager_expr_from_flat_schema(flat_schema):
    """Builds an eager expression from a flattened schema."""
    result = []
    for path, join_method in flat_schema.items():
        if join_method == JOINED:
            result.append(joinedload(path))
        elif join_method == SUBQUERY:
            result.append(subqueryload(path))
        else:
            raise ValueError(f'Bad join method `{join_method}` in `{path}`')
    return result


class EagerLoadMixin(SessionMixin):
    """Implements eager loading functionality."""
    __abstract__ = True

    @classmethod
    def with_(cls, schema: dict):
        """Query class and eager load schema at once.

        Example:
            schema = {
                'user': JOINED, # joinedload user
                'comments': (SUBQUERY, {  # load comments in separate query
                    'user': JOINED  # but, in this separate query, join user
                })
            }

            # the same schema using class properties:
            schema = {
                Post.user: JOINED,
                Post.comments: (SUBQUERY, {
                    Comment.user: JOINED
                })
            }
            User.with_(schema).first()

        Args:
            schema: to be documented
        Returns:
            to be documented
        """
        return cls.query.options(*eager_expr(schema or {}))

    @classmethod
    def with_joined(cls, *paths: Union[List[str], List[InstrumentedAttribute]]):
        """Eager loading for simple cases.

        Eager loading for simple cases where we need to just
        joined load some relations.
        In strings syntax, you can split relations with dot
        due to this SQLAlchemy feature: https://goo.gl/yM2DLX

        Example 1:
            Comment.with_joined('user', 'post', 'post.comments').first()

        Example 2:
            Comment.with_joined(Comment.user, Comment.post).first()

        Args:
            paths: to be documented

        Returns:
            to be documented
        """
        options = [joinedload(path) for path in paths]
        return cls.query.options(*options)

    @classmethod
    def with_subquery(cls, *paths: Union[List[str], List[InstrumentedAttribute]]):
        """Eager loading for simple cases.

        Eager loading for simple cases where we need to just
        joined load some relations.
        In strings syntax, you can split relations with dot
         (it's SQLAlchemy feature)

        Example 1:
            User.with_subquery('posts', 'posts.comments').all()

        Example 2:
            User.with_subquery(User.posts, User.comments).all()

        Args:
            paths: to be documented

        Returns:
            to be documented
        """
        options = [subqueryload(path) for path in paths]
        return cls.query.options(*options)
