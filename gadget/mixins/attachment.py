# -*- coding: utf-8 -*-
"""This module contains a mixin that is for attachments."""
import inspect
from typing import List
from typing import Tuple

from sqlalchemy import event
from sqlalchemy.orm import reconstructor
from sqlalchemy.orm import Session
from sqlalchemy.orm import SessionTransaction

import gadget.models
from gadget import ActiveRecordMixin
# from gadget.models import Artifact

# pylint: disable=arguments-differ


class AttachmentMixin(ActiveRecordMixin):
    """This mixin implements functionality needed if an object has attachments."""
    __abstract__ = True

    def __init__(self, *args, **kwargs):
        """Constructor of AttachmentMixin."""
        super().__init__(*args, **kwargs)
        self._changes = []
        """Marks this mixin as being abstract, thus having no associated table"""

    @reconstructor
    def init_on_load(self) -> None:
        """Initializes loaded objects."""
        self._changes = []

    def __setattr__(self, attribute, value) -> None:
        """Sets attribute values at an object.

        This function not only sets attribute values but also
        tracks changes that are made to them.

        Args:
            attribute: attribute whose value is to be set
            value: the attribute value to be set to
        """
        if attribute == '_changes' and hasattr(self, '_changes'):
            return

        try:
            current = getattr(self, attribute)  # get current value of field
            if current is not None and current != value:
                self._changes.append((attribute, getattr(self, attribute), value))
        except AttributeError:
            pass
        object.__setattr__(self, attribute, value)

    @property
    def has_changes(self) -> bool:
        """Indicates whether this object has been changed.

        Returns:
            True if changes have been made to this object, otherwise False
        """
        if self._changes:
            return True
        return False

    def attachment_attributes(self) -> List[Tuple[str, gadget.models.Artifact]]:
        """Get all attachment attributes.

        Returns:
            all attachment attributes
        """
        return inspect.getmembers(self, lambda a: isinstance(a, gadget.models.Artifact))

    def _delete_all_attachments(self) -> None:
        """Delete all attachments of the object.

        This method deletes all attachments that are stored
        in the external storage.
        """
        for _, attachment in self.attachment_attributes():
            attachment.delete()

    def _delete_orpahned_attachments(self) -> None:
        """Removes orphaned/stale attachments of this object.

        If an attachment is overwritten and thus replaced by a new
        attachment, the old attachment would still remain in the
        external storage. To maintain data consistency between the
        external storage (e.g. S3) and the database used
        (e.g. Postgres), the old attachment value must be deleted
        from the external storage. This is achieved with this method.

        It iterates through all changes made to the object and
        in case an attachment value was updated deletes the old
        attachment value from the external storage.
        """
        if self.has_changes:
            for _, old_value, _ in self._changes:
                if isinstance(old_value, gadget.models.Artifact):
                    old_value.delete()
            self._changes = []

    def bind_events(self) -> None:
        """Binds the required event on sqlalchemy session to handle rollbacks."""
        # event.listen(Session, "before_commit", self.on_before_commit)
        # event.listen(Session, "after_attach", self.on_attach)
        # event.listen(self.session, 'after_commit', self.on_commit)
        event.listen(self.session, 'after_soft_rollback', self.on_rollback)
        # event.listen(self.session, 'persistent_to_deleted', self.on_delete)
        # event.listen(self.session, 'persistent_to_detached', self.on_close)

    def unbind_events(self) -> None:
        """Unbinds events from sqlalchemy.

        Reverts the bindings made in the :meth:`bind_events`
        function call
        """
        # event.remove(self.session, 'after_commit', self.on_commit)
        event.remove(self.session, 'after_soft_rollback', self.on_rollback)
        # event.remove(self.session, 'persistent_to_deleted', self.on_delete)

    # def on_commit(self, session: Session):
    #     for elem in session:
    #         print(elem)
    #
    # def on_attach(self, session, instance):
    #     print(f'Attached instance is: {instance}')
    #
    # def on_close(self, session, instance):
    #
    #     print(f'On close: Attached instance is: {instance.vals}')
    #
    # def on_before_commit(self, session):
    #     for elem in session:
    #         print(elem)
    #     print("This gets executed before each commit!")

    def on_rollback(self, session: Session, previous_transaction: SessionTransaction) -> None:
        # pylint: disable=unused-argument
        """Executed during rollback.

        Args:
            session: to be documented
            previous_transaction: to be documented
        """
        self._delete_orpahned_attachments()

    def attach(self, attribute: str, artifact: bytes, uri: str, **kwargs):
        """Attach an artifact to an attribute field.

        Args:
            attribute: the attribute the artifact should be assigned to
            artifact: the artifact to be stored
            uri: the uri that determines where the artifact is to be stored.
            **kwargs: additional metadata that should be recorded with the artifact

        Returns:
            the attached artifact
        """
        artifact = gadget.models.Artifact.create(artifact=artifact,
                                                 uri=uri,
                                                 attached_to=self.id,
                                                 **kwargs)
        setattr(self, attribute, artifact)
        return artifact

    def save(self, commit: bool = True, keep_orphans: bool = False):
        """Saves the object to the database.

        Args:
            commit: if True, the session will also be committed
                default: True
            keep_orphans: if True, artifact attachments that become
                orphaned will not be deleted. default: True

        Returns:
            the object itself
        """
        self.bind_events()
        self.session.add(self)
        if commit:
            self.session.commit()
        if not keep_orphans:
            self._delete_orpahned_attachments()
        self.unbind_events()
        return self

    def update(self, commit: bool = True, **kwargs):
        """Updates fields of this object.

        Args:
            commit: if True, changes are persisted in the database

        Returns:
            the updated object
        """
        for field_name, value in kwargs.items():
            # TODO: log accesses
            if field_name in self.settable_attributes and \
               field_name not in self.__class__.readonly_attributes:
                setattr(self, field_name, value)
            if commit:
                self.save()
        return self

    def delete(self, field: str = None, keep_orphans: bool = False) -> None:
        """Delete specific attachment or entire the object.

        If a field is specified only this field will be deleted. Should
        the deleted field be an attachment field, then the associated
        attachment will be removed unless keep_orphans is set to True.

        If no field is specified the entire object will be removed,
        including all attachments.

        Args:
            field: the attachment field to be deleted
            keep_orphans: if True, artifact attachments that become
                orphaned will not be deleted. default: True
        """
        if field is not None:
            setattr(self, field, None)
            self.save()
            if not keep_orphans:
                self._delete_orpahned_attachments()
        else:
            self.session.delete(self)
            self.session.commit()
            if not keep_orphans:
                self._delete_all_attachments()
