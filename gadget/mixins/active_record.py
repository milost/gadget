# -*- coding: utf-8 -*-
"""This module contains the active record mixin."""
from typing import List
from typing import Set

from .inspection import InspectionMixin
from .load import EagerLoadMixin
from .serializable import Serializable
from .session import SessionMixin
from gadget.exceptions import ModelNotFoundError
from gadget.utils import classproperty

# pylint: disable=all


class ActiveRecordMixin(InspectionMixin, EagerLoadMixin, SessionMixin, Serializable):
    """This mixin implements the Active Record design pattern."""
    __abstract__ = True

    readonly_attributes: Set[str] = set()
    """
    These attributes will be excluded from the settable attributes property.
    They can only be set at object creation and not changed afterwards.
    This should contain the primary key column of a table, since they can't be changed.
    """
    @classproperty
    def settable_attributes(cls) -> List[str]:
        """Gets attributes that can be updated after object creation.

        By default these are all columns.

        Returns:
            list of attribute/column names that can be updated (written to) after object creation
        """
        all_attributes = set(cls.columns + cls.hybrid_properties + cls.settable_relations)
        return list(all_attributes.difference(cls.readonly_attributes))

    def save(self, commit: bool = True):
        """Saves the object to the database.

        Args:
            commit: if True, the session will also be committed

        Returns:
            the object itself
        """
        self.session.add(self)
        if commit:
            self.session.commit()
        return self

    @classmethod
    def create(cls, **kwargs):
        """Create and persist a new record for the model.

        Args:
            kwargs: attributes for the record

        Returns:
            the new model instance
        """
        return cls(**kwargs).save()

    def update(self, is_serialized: bool = False, commit: bool = True, **kwargs):
        """Updates fields of this instance.

        If the serialized flag is set, the fields are first deserialized using the
        objects marshmallow schema.

        Args:
            is_serialized: if True, indicates that the data needs
                to be deserialized via marshmallow first
            commit: if True, changes are persisted in the database

        Returns:
            the updated object
        """
        # TODO: merge with update and add commit flag
        for field_name, value in kwargs.items():
            # TODO: log bad accesses
            if field_name in self.settable_attributes and field_name not in self.__class__.readonly_attributes:
                if is_serialized:
                    value = self.schema.declared_fields[field_name].deserialize(value)
                setattr(self, field_name, value)
            if commit:
                self.save()
        return self

    def delete(self, commit: bool = True):
        """Removes the model from the current entity session and marks it for deletion."""
        self.session.delete(self)
        if commit:
            self.session.commit()

    @classmethod
    def delete_by_id(cls, *ids):
        """Delete the records with the given ids.

        Args:
            ids: ids of records that should be deleted
        """
        for pk in ids:
            obj = cls.fetch(pk)
            if obj is not None:
                obj.delete(commit=False)
        cls.commit()

    @classmethod
    def all(cls):
        """Retrieve all objects of the class.

        Returns:
            all objects of the class.
        """
        return cls.query.all()

    @classmethod
    def where(cls, **filters):
        """Retrieves all objects that match the specified filter criteria.

        Args:
            filters: the properties to filter by

        Returns:
            all objects that match the specified filter criteria.
        """
        return cls.query.filter_by(**filters)

    @classmethod
    def fetch(cls, _id):
        """Retrieve object by its primary key.

        Args:
            _id: the primary key of the object that should be retrieved

        Returns:
            the object with the corresponding primary key
        """
        return cls.query.get(_id)

    @classmethod
    def fetch_or_fail(cls, _id):
        """Retrieve object by its primary key.

        Same like the fetch method, but raises a ModelNotFoundError exception
        if there is no object that matches the passed _id.

        Args:
            _id: the primary key of the object that should be retrieved

        Returns:
            the object with the corresponding primary key
        """
        # assume that query has custom get_or_fail method
        result = cls.fetch(_id)
        if result:
            return result
        raise ModelNotFoundError(f"{cls.__name__} with id '{_id}' was not found")
