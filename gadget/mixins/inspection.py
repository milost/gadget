# -*- coding: utf-8 -*-
"""This module contains the introspection mixin."""
from typing import List

from sqlalchemy import inspect
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_method
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import ColumnProperty
from sqlalchemy.orm import RelationshipProperty

from gadget.utils import classproperty

# pylint: disable=E0213, C0123

Base = declarative_base()


class InspectionMixin(Base):
    """This mixin enables the inspection of the underlying table representation."""
    __abstract__ = True

    @classproperty
    def columns(cls):
        """Get column names of underlying table.

        Returns:
            column names of underlying table representation.
        """
        return inspect(cls).columns.keys()

    @classproperty
    def primary_keys_full(cls) -> List[ColumnProperty]:
        """Get primary key properties for a SQLAlchemy class."""
        mapper = cls.__mapper__
        return [mapper.get_property_by_column(column) for column in mapper.primary_key]

    @classproperty
    def primary_keys(cls):
        """Get primary keys of underlying table representation.

        Returns:
            a list of primary keys
        """
        return [pk.key for pk in cls.primary_keys_full]

    @classproperty
    def relations(cls) -> List[str]:
        """Returns a `list` of relationship names for the given model."""
        return [
            c.key for c in cls.__mapper__.iterate_properties
            if isinstance(c, RelationshipProperty)
        ]

    @classproperty
    def settable_relations(cls):
        """Return a `list` of relationship names or the given model."""
        return [r for r in cls.relations if getattr(cls, r).property.viewonly is False]

    @classproperty
    def hybrid_properties(cls):
        """Return a list of all hybrid properties."""
        items = inspect(cls).all_orm_descriptors
        return [item.__name__ for item in items if type(item) == hybrid_property]

    @classproperty
    def hybrid_methods_full(cls):
        """Return a list of all hybrid properties."""
        items = inspect(cls).all_orm_descriptors
        return {item.func.__name__: item for item in items if type(item) == hybrid_method}

    @classproperty
    def hybrid_methods(cls):
        """Return a list of all hybrid methods."""
        return list(cls.hybrid_methods_full.keys())
