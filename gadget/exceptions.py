# -*- coding: utf-8 -*-
"""This module defines multiple custom exception classes."""
# pylint: disable=W0231, E1003, W0613


class NoSessionError(RuntimeError):
    """To be documented."""


class NoStorageError(RuntimeError):
    """To be documented."""


class NoCacheError(RuntimeError):
    """To be documented."""


class ModelNotFoundError(ValueError):
    """To be documented."""


class ListError(RuntimeError):
    """To be documented."""


class BucketNotFound(Exception):
    """Is raised when bucket does not exist."""
    def __init__(self, *args, **kwargs):
        """Constructor of exception."""


class ArtifactNotFound(Exception):
    """Is raised when artifact does not exist."""
    def __init__(self, *args, **kwargs):
        """Constructor of exception."""


class DataNotFoundError(FileNotFoundError):
    """Is raised when data cannot be found in bucket."""
    def __init__(self, *args, **kwargs):
        """Constructor of exception."""
        super(FileNotFoundError, self).__init__()
