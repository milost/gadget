# -*- coding: utf-8 -*-
"""This module contains a number of utility functions."""
import mimetypes
from functools import reduce
from typing import List
from typing import Optional

# from elasticsearch_dsl import Search

# pylint: disable=R1705


class classproperty:
    """A class property decorator."""
    def __init__(self, fget):
        """Constructor of the class property decorator.

        Args:
            fget: the field that should be decorated decorator.
        """
        self.fget = fget

    def __get__(self, owner_self, owner_cls):
        """Get property of class.

        Args:
            owner_self: to be documented
            owner_cls: the class from which the property is to be retrieved

        Returns:
            the owner class of a property
        """
        return self.fget(owner_cls)


def all_subclasses(cls):
    """Retrieve all subclasses of the given class.

    Args:
        cls: the class from which all subclasses are to be retrieved.

    Returns:
        all subclasses of the given class
    """
    return cls.__subclasses__() + [g for s in cls.__subclasses__() for g in all_subclasses(s)]


def guess_extension(mimetype: Optional[str]) -> str:
    """Guess file ending (file extension) from a given mimetype.

    Example:
        #. None => '.bin'
        #. 'image/jpeg' => '.jpg'

    Args:
        mimetype: the mimetype for which an extension should be determined

    Returns:
        string: a guessed file ending (file extension)
    """
    result = mimetypes.guess_extension(mimetype)

    if mimetype is None:
        result = '.bin'
    elif mimetype == 'image/jpeg':
        result = '.jpg'

    return result


def split_camel(text) -> List[str]:
    """Split string according to camel case notation.

    Example:
        * 'CharSpanAnnotation' -> ['Char', 'Span', 'Annotation']
        * 'PathURLFinder' -> ['Path', 'URL', 'Finder']

    Args:
        text: the string that should be splitted according to camel case notation

    Returns:
        List[str]: a list containing the splitted strings
    """
    def splitter(text, char):
        if len(text) <= 1:  # To avoid adding a wrong space in the beginning
            return text + char
        if char.isupper() and text[-1].islower():  # Regular Camel case
            return text + " " + char
        elif text[-1].isupper() and char.islower(
        ) and text[-2] != " ":  # Detect Camel case in case of abbreviations
            return text[:-1] + " " + text[-1] + char
        else:  # Do nothing part
            return text + char

    converted_text = reduce(splitter, text, "")
    return converted_text.split(" ")


# def slice_search(search: Search, offset: int = None, limit: int = None):
#     """
#     Slice an elasticsearch search object (i.e. jump to a specific "page" of the index).
#
#     Args:
#         search: the actual search object
#         offset: if set it defines the starting index
#           (i.e., if set to 10 the first 10 documents will be skipped)
#         limit: if set limits the number of entities returned
#     """
#     if offset is not None and offset > 0 and limit is not None and limit > 0:
#         search = search[offset:offset + limit]
#     elif offset is not None and offset > 0:
#         search = search[offset:]
#     elif limit is not None and limit > 0:
#         search = search[:limit]
#     return search
