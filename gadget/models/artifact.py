# -*- coding: utf-8 -*-
"""This module contains the Artifact and ArtifactLocation classes."""
from __future__ import annotations

import uuid
from pathlib import Path
from typing import Any

from sqlalchemy.ext.mutable import MutableDict

from gadget.storage.manager import StorageManager
from gadget.storage.storage import Storage


class ArtifactLocation:
    """The ArtifactLocation class manages the location of stored artifacts."""
    def __init__(
        self,
        bucket: str,
        filename: str,
        path: str | None = None,
        prefix: list[str] | None = None,
        path_separator="/",
        meta_delimiter="_-_",
    ):
        """Constructor.

        Args:
            bucket: the bucket used
            path: the path within the bucket
            filename: the artifacts name
            prefix: a prefix to be set before the filename
            path_separator: the separator used, default: /
            meta_delimiter: the delimiter used to separate
                filename and metadata, default: _-_
        """
        self.meta_delimiter = meta_delimiter
        self.path_separator = path_separator
        self.bucket = bucket
        self.filename = filename
        self.path = path or None
        self.prefix = prefix or []

        # get extension of filename
        filename = Path(self.filename)
        if filename.suffixes:
            self.extension = "".join(filename.suffixes)
        else:
            self.extension = None

    @property
    def artifact_name(self):
        """The original filename of the artifact.

        Returns:
            the original filename of the artifact.
        """
        name = []

        if self.prefix:
            name.append("_".join(self.prefix))
        name.append(self.filename)

        return self.meta_delimiter.join(name)

    @property
    def artifact_path(self) -> str:
        """Returns the full artifact path.

        This property returns the full path to an artifact,
        including the artifact name itself.

        Returns:
            the full artifact path.
        """
        if self.path is not None:
            return f"{self.path}{self.path_separator}{self.artifact_name}"
        return self.artifact_name

    @classmethod
    def from_uri(cls,
                 uri: str,
                 path_separator: str = "/",
                 meta_delimiter: str = "_-_") -> ArtifactLocation:
        """Creates an ArtifactLocation from a given uri.

        Args:
            uri: the uri used for creation
            path_separator: the separator used in the uri

        Returns:
            an ArtifactLocation instance
        """
        location_parts = uri.split(path_separator)
        artifact_name = location_parts[-1]

        artifact_parts = artifact_name.split(meta_delimiter)
        if len(artifact_parts) == 1:
            prefix = []
            filename = artifact_parts[0]
        elif len(artifact_parts) == 2:
            prefix, filename = artifact_parts
            prefix = prefix.split("_")
        else:
            raise ValueError("Found more then one meta data delimiter in artifact name!")

        return cls(bucket=location_parts[0],
                   path=path_separator.join(location_parts[1:-1]),
                   prefix=prefix,
                   filename=filename)

    def __repr__(self):
        """Returns the components of an ArtifactLocation instance.

        Returns:
            the components of an ArtifactLocation instance
        """
        return f"[{self.bucket}, {self.path}, {self.prefix}, {self.filename}, {self.extension}]"

    def __str__(self):
        """A string representation of an ArtifactLocation.

        Returns:
            A string representation of an ArtifactLocation.
        """
        uri = [self.bucket]

        if self.path is not None:
            uri.append(self.path)

        uri.append(self.artifact_name)

        return self.path_separator.join(uri)

    @property
    def uri(self):
        """Return the string version of the location."""
        return str(self)


class Artifact(MutableDict):
    """This class represents an attachable artifact."""
    @property
    def uri(self) -> ArtifactLocation:
        """Resource identifier of the artifact.

        Returns:
            object with information about the artifact location.
        """
        return ArtifactLocation(bucket=self["bucket"],
                                path=self["path"],
                                prefix=self["prefix"],
                                filename=self["filename"])

    @uri.setter
    def uri(self, uri: str | ArtifactLocation) -> None:
        if isinstance(uri, str):
            uri = ArtifactLocation.from_uri(uri)

        self["bucket"] = uri.bucket
        self["path"] = uri.path
        self["prefix"] = uri.prefix
        self["filename"] = uri.filename
        self["artifact_name"] = uri.artifact_name
        self["extension"] = uri.extension

    @property
    def artifact_name(self) -> str:
        """The filename used to store the attachment in the storage.

        Format::
            '{self.__prefix__}-{self.key}{self.suffix}{if self.extension
            else ''}'

        Returns:
            filename used to store the attachment in the storage.
        """
        return self.uri.artifact_name

    @property
    def bucket(self) -> str:
        """Returns the name of the bucket the artifact is stored in."""
        return self.uri.bucket

    @property
    def original_filename(self) -> str:
        """Returns the original file name.

        Returns:
            the original filename
        """
        return self.uri.filename

    @property
    def extension(self) -> str:
        """The file extension of the artifact.

        Returns:
            the file extensions of the artifact.
        """
        return self.uri.extension

    @property
    def storage_id(self) -> str:
        # pylint: disable=protected-access
        """Returns the storage id of the used storage backend."""
        return self.get("store_id", StorageManager._default_storage_backend)

    @property
    def identifier(self):
        """Return a unique identifier of this artifact."""
        return self.get("identifier")

    @identifier.setter
    def identifier(self, value) -> None:
        self["identifier"] = value

    @property
    def content_type(self):
        """Return the content type of this artifact."""
        return self.get("content_type")

    @content_type.setter
    def content_type(self, value) -> None:
        self["content_type"] = value

    @property
    def storage(self):
        """Returns the storage backend which this file is stored on.

        Returns:
             the storage backend which this file is stored on.
        """
        return StorageManager.get(self.storage_id)

    def get_store(self) -> Storage:
        """Returns the storage backend which this file is stored on.

        Returns:
             the storage backend which this file is stored on.
        """
        return StorageManager.get(self.storage_id)

    @classmethod
    def create(cls, artifact, uri: str, attached_to: str, *args, **kwargs):
        """Factory method to create and attach an artifact.

        Args:
            artifact: the object to be stored.
            uri: the uri pointing to where the data should be stored
            attached_to: the id of the database object the artifact is attached to
            args: The same as the :meth:`.attach`
            kwargs: The same as the :meth:`.attach`

        Returns:
            an instance of this class.
        """
        instance = cls()
        return instance.attach(artifact, uri, attached_to, *args, **kwargs)

    def delete(self) -> None:
        """Deletes the file.

        .. warning:: This operation can not be roll-backed. So if you want to
                     delete a file, just set it to :const:`None` or set it by
                     new :class:`.Attachment` instance, while passed
                     ``delete_orphan=True`` in :class:`.StoreManager`.
        """
        self.get_store().delete(str(self.uri))

    def replace(self, content: bytes) -> None:
        """Replace content of stored artifact.

        Args:
            content: the new content of the artifact
        """
        metadata = self.storage.metadata(self.uri)
        self.storage.put(self.uri, content, metadata=metadata)

    def attach(
        self,
        artifact,
        uri: str | ArtifactLocation,
        attached_to: str,
        store_id: str = None,
        content_type: str = "application/octet-stream",
        **kwargs,
    ):
        """Creates an artifact attachment.

        This method creates an attachment object. Part of this process is
        to upload the attachment object to an external storage, such as S3.

        Args:
            artifact: the object to be attached.
            uri: the location where the artifact should be stored.
            attached_to: the id of the database entry that this attachment belongs to.
            store_id: the id of the storage backend that should be used.
                By default, the default storage is used as configured in the StorageManager.
            content_type: the content type of the attachment.
            **kwargs: additional information that should be added as metadata
        """
        # create UUID for the artifact attachment
        self.identifier = str(uuid.uuid4())

        if isinstance(uri, str):
            location = ArtifactLocation.from_uri(uri)
            location.prefix = [self.__class__.__name__.lower(), self.identifier]
            self.uri = location
        else:
            self.uri = uri
        self.content_type = content_type

        info = kwargs.copy()
        info.update(store_id=store_id or self.storage_id)

        # remove all entries where value is None
        self.update([(k, v) for k, v in info.items() if v is not None])

        metadata = {"artifact_id": self.identifier, "attached_to": str(attached_to)}
        self.storage.put(self.uri, artifact, metadata=metadata)

        return self

    @property
    def data(self) -> bytes:
        """Get the stored data.

        Returns:
            the stored data.
        """
        return self.get_store().get(self.uri)

    def __eq__(self, other: Any) -> bool:
        """Check if two artifacts are equal.

        Args:
            other: the object to compare to

        Returns:
            True if the two object are equal, else False
        """
        if not isinstance(other, Artifact):
            return False
        return self.identifier == other.identifier

    def __hash__(self) -> int:
        """Returns a unique hash of this artifact based on :attr:`.identifier`."""
        return hash(self.identifier)
