# -*- coding: utf-8 -*-
"""The models module contains all model classes that are used in extending SQLAlchemy."""
from .artifact import Artifact
from .artifact import ArtifactLocation

__all__ = ["Artifact", "ArtifactLocation"]
