# gadget

An easy to use ORM implementation based on SQL Alchemy

## Project setup

1. Make sure [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) are installed.
2. If you also want to easily update your pyenv installation, make sure you install [pyenv-update](https://github.com/pyenv/pyenv-update) too
3. Clone repository `git clone https://gitlab.com/milost/gadget`
4. Set up the project by running: `make init for=development` for development or `make init` for production dependencies
5. You can see a list of all supported actions by running `make`

## Tooling setup

This section describes the settings that should be made to contribute in accordance with the development rules for this project.

### Code formatting with [yapf](https://github.com/google/yapf#introduction)

This project uses the [yapf](https://github.com/google/yapf#introduction) formatter to format the written code. This formatter formats all files according to the specification given in `setup.cfg` file. After all project dependencies have been installed, the formatter can be executed by executing the command

    make format

#### yapf with Visual Stutio Code

Integrating yapf into Visual Studio Code is relatively easy. Just go to `Preferences -> Settings` and add the following two entries to your configuration:

```json
{
    "python.formatting.provider": "yapf",
    "python.formatting.yapfPath": "[Path to the yapf executable]"
}
```

In Linux and Unix based systems the path to a command can easily be determined by using the `which` command i.e. `which yapf`

## Make targets

The project defines a set of make targets which are listed below:

### Setup targets

| Target              | Description                                                                                                       |
| ------------------- | :---------------------------------------------------------------------------------------------------------------- |
| `init for=[env]`    | Initialize the project from scratch                                                                               |
| `install-pyenv`     | Install pyenv and plugins (experimental)                                                                          |
| `environment`       | Create virtual environment for project                                                                            |
| `install [env=dev]` | Install project dependencies                                                                                      |
| `activate`          | Activate the current project environment                                                                          |
| `drop-environment`  | Delete the virtual environment for this project                                                                   |

### Development targets

| Target                               | Description                                                                   |
| ------------------------------------ | :---------------------------------------------------------------------------- |
| `run`                                | Execute the main.py of the project                                            |
| `lint`                               | Run linter ([flake8](https://flake8.pycqa.org/en/latest/)) over project files |
| `format`                             | Run formatter ([yapf](https://github.com/google/yapf))                        |
| `analyze`                            | Run full analysis using [pre-commit](https://pre-commit.com/)                 |
| `update [env=dev]`                   | Update project dependencies                                                   |
| `release [part=[major,minor,patch]]` | Increment package version                                                     |

### Test targets

| Target                             | Description                               |
| ---------------------------------- | :---------------------------------------- |
| `test [coverage=true] [show=true]` | Run tests quickly with the default Python |

### Build targets

| Target  | Description                             |
| ------- | :-------------------------------------- |
| `build` | Build `sdist` and `wheel` distributions |
| `sdist` | Build `sdist` distribution              |
| `wheel` | Build `wheel` distribution              |

### Clean targets

| Target        | Description                                   |
| ------------- | :-------------------------------------------- |
| `clean`       | Clean _build_, _python_, and _test_ artifacts |
| `clean-build` | Clean _build_ artifacts                       |
| `clean-pyc`   | Clean _python_ artifacts                      |
| `clean-test`  | Clean _test_ artifacts                        |

### Documentation targets

| Target             | Description                                            |
| ------------------ | :----------------------------------------------------- |
| `docs [show=true]` | Generate Sphinx HTML documentation, including API docs |
