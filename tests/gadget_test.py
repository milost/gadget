# -*- coding: utf-8 -*-
"""This module implements the tests for the gadget package."""


def test_inc():
    """A simple dummy test."""
    # Setup
    origin = 4
    expected = 4

    # Execute

    # Verify
    assert origin == expected
