# -*- coding: utf-8 -*-
"""This module contains pytest configuration elements."""
import os

here = os.path.dirname(os.path.realpath(__file__))
# Contains the path to the current directory

pytest_plugins = []
# Contains a list of registered pytest plugins


def _as_module(root_path: str, path: str) -> str:
    """Returns the package of the fixture.

    Args:
        root_path: the root path of the fixture.
        path: the path of the fixture itself.

    Returns:
        the package of the fixture.
    """
    path = os.path.join(root_path, path)
    path = path.replace(here, "")
    path = path.replace(".py", "")
    path = path.replace(os.path.sep, ".")[1:]
    return "tests." + path


for root, dirs, files in os.walk(here, topdown=True):
    # Discovers and registers all fixtures declared in this project.
    dirs[:] = [d for d in dirs if d.startswith("fixtures")]
    pytest_plugins += [_as_module(root, f) for f in files if f.endswith("fixtures.py")]
