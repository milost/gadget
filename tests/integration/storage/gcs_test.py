# -*- coding: utf-8 -*-
"""This module contains integration tests for GCS backend."""
import pytest
from assertpy import assert_that
from google.cloud.storage import Blob
from google.cloud.storage.client import Client as GClient

from gadget.storage.gcs import GCStorage


@pytest.mark.integration
def test_constructor():
    """Tests constructor of GCS backend."""
    storage = GCStorage()
    assert storage.client is None


@pytest.mark.integration
def test_connection():
    """Tests connection to Google Cloud Storage."""
    storage = GCStorage()
    storage.connect()
    assert storage.client is not None


@pytest.mark.integration
def test_isconnected():
    """Tests connection to Google Cloud Storage."""
    storage = GCStorage()
    storage.connect()
    assert storage.is_connected


@pytest.mark.integration
def test_create_bucket():
    """Tests bucket creation."""
    storage = GCStorage()
    storage.connect()

    storage.create_bucket('milost-gadget-gcs-test-1')

    # check if bucket exists
    client: GClient = storage.client
    buckets = [bucket.name for bucket in client.list_buckets()]
    assert 'milost-gadget-gcs-test-1' in buckets

    # delete bucket
    bucket = client.get_bucket('milost-gadget-gcs-test-1')
    bucket.delete()


@pytest.mark.integration
def test_delete_bucket():
    """Tests bucket deletion."""
    storage = GCStorage()
    storage.connect()

    storage.create_bucket('milost-gadget-gcs-test-1')

    client: GClient = storage.client
    # check if bucket exists
    buckets = [bucket.name for bucket in client.list_buckets()]
    assert 'milost-gadget-gcs-test-1' in buckets

    storage.delete_bucket('milost-gadget-gcs-test-1')

    # check if bucket was deleted
    buckets = [bucket.name for bucket in client.list_buckets()]
    assert 'milost-gadget-gcs-test-1' not in buckets


@pytest.mark.integration
def test_bucket_exists():
    """Tests bucket exists."""
    storage = GCStorage()
    storage.connect()

    storage.create_bucket('milost-gadget-gcs-test-1')

    assert storage.bucket_exists('milost-gadget-gcs-test-1')

    storage.delete_bucket('milost-gadget-gcs-test-1')

    assert not storage.bucket_exists('milost-gadget-gcs-test-1')


@pytest.mark.integration
def test_lookup_bucket():
    """Tests lookup bucket."""
    storage = GCStorage()
    storage.connect()

    bucket = storage.lookup_bucket('milost-gadget-gcs-test-1')
    assert bucket is None

    storage.create_bucket('milost-gadget-gcs-test-1')

    bucket = storage.lookup_bucket('milost-gadget-gcs-test-1')
    assert bucket is not None

    storage.delete_bucket('milost-gadget-gcs-test-1')


@pytest.mark.integration
def test_put_data():
    """Tests putting data into a bucket."""
    storage = GCStorage()
    storage.connect()
    client: GClient = storage.client

    storage.create_bucket('milost-gadget-gcs-test-1')

    storage.put('milost-gadget-gcs-test-1/test.txt', b'This is a test!', content_type='text/plain')

    # check if file was placed correctly
    bucket = client.bucket('milost-gadget-gcs-test-1')

    blob = bucket.get_blob('test.txt')
    assert blob is not None
    content = blob.download_as_bytes()
    assert content == b'This is a test!'

    blob.delete()
    storage.delete_bucket('milost-gadget-gcs-test-1')


@pytest.mark.integration
def test_get_data():
    """Tests getting data from an object."""
    storage = GCStorage()
    storage.connect()
    client: GClient = storage.client

    # # Create a bucket
    bucket = client.create_bucket(bucket_or_name='milost-gadget-gcs-test-1', location='us-east1')
    blob = Blob('test.txt', bucket)
    blob.upload_from_string('This is a test!')

    content = storage.get('milost-gadget-gcs-test-1/test.txt')
    assert content == b'This is a test!'

    blob.delete()
    bucket.delete()


@pytest.mark.integration
def test_contains():
    """Tests if uri exists in storage."""
    storage = GCStorage()
    storage.connect()
    client: GClient = storage.client

    # # Create a bucket
    bucket = client.create_bucket(bucket_or_name='milost-gadget-gcs-test-1', location='us-east1')
    blob = Blob('test.txt', bucket)
    blob.upload_from_string('This is a test!')

    assert 'milost-gadget-gcs-test-1/test.txt' in storage
    assert 'milost-gadget-gcs-test-1/foobar.txt' not in storage

    blob.delete()
    bucket.delete()


@pytest.mark.integration
def test_delete():
    """Tests deletion of object."""
    storage = GCStorage()
    storage.connect()
    client: GClient = storage.client

    # # Create a bucket
    bucket = client.create_bucket(bucket_or_name='milost-gadget-gcs-test-1', location='us-east1')
    Blob('test.txt', bucket).upload_from_string('This is a test!')
    Blob('foobar.txt', bucket).upload_from_string('This is another test!')

    assert 'milost-gadget-gcs-test-1/test.txt' in storage
    assert 'milost-gadget-gcs-test-1/foobar.txt' in storage

    storage.delete('milost-gadget-gcs-test-1/test.txt')

    assert 'milost-gadget-gcs-test-1/test.txt' not in storage
    assert 'milost-gadget-gcs-test-1/foobar.txt' in storage

    storage.delete('milost-gadget-gcs-test-1/foobar.txt')

    assert 'milost-gadget-gcs-test-1/test.txt' not in storage
    assert 'milost-gadget-gcs-test-1/foobar.txt' not in storage

    bucket.delete()


@pytest.mark.integration
def test_delete_multiple():
    """Tests deletion of multiple objects."""
    storage = GCStorage()
    storage.connect()
    client: GClient = storage.client

    # # Create a bucket
    bucket = client.create_bucket(bucket_or_name='milost-gadget-gcs-test-1', location='us-east1')
    Blob('test.txt', bucket).upload_from_string('This is a test!')
    Blob('foobar.txt', bucket).upload_from_string('This is another test!')

    assert 'milost-gadget-gcs-test-1/test.txt' in storage
    assert 'milost-gadget-gcs-test-1/foobar.txt' in storage

    storage.delete(['milost-gadget-gcs-test-1/test.txt', 'milost-gadget-gcs-test-1/foobar.txt'])

    assert 'milost-gadget-gcs-test-1/test.txt' not in storage
    assert 'milost-gadget-gcs-test-1/foobar.txt' not in storage

    bucket.delete()


@pytest.mark.integration
def test_get_metadata():
    """Tests the retrieval of metadata."""
    storage = GCStorage()
    storage.connect()
    client: GClient = storage.client

    # # Create a bucket
    bucket = client.create_bucket(bucket_or_name='milost-gadget-gcs-test-1', location='us-east1')
    blob_1 = Blob('test.txt', bucket)
    blob_1.metadata = {'foo': 'bar'}
    blob_1.upload_from_string('This is a test!')

    blob_2 = Blob('foobar.txt', bucket)
    blob_2.upload_from_string('This is another test!')

    metadata = storage.metadata('milost-gadget-gcs-test-1/test.txt')
    assert_that(metadata).is_equal_to({'foo': 'bar'})

    metadata = storage.metadata('milost-gadget-gcs-test-1/foobar.txt')
    assert metadata is None

    blob_1.delete()
    blob_2.delete()
    bucket.delete()
