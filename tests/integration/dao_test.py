# -*- coding: utf-8 -*-
"""This module contains integration tests for the DAO class."""
import pytest
from assertpy import assert_that
from sqlalchemy import Column
from sqlalchemy import JSON
from sqlalchemy import String
from sqlalchemy import text

from gadget.extensions.type import GUID
from gadget.mixins.attachment import AttachmentMixin
from gadget.models import Artifact


class Document(AttachmentMixin):
    """A simple object used for testing the DAO."""
    __tablename__ = 'documents'

    id = Column(GUID, primary_key=True)
    stuff = Column(String)
    file = Column(Artifact.as_mutable(JSON))
    content = Column(Artifact.as_mutable(JSON))

    def __repr__(self):
        """String representation of the document.

        Returns:
            a string representation of the document.
        """
        return f"<{self.file} id={self.id}>"


@pytest.mark.integration
def test_save_basic_document_creation(dao):
    """Tests basic constructor of GCS backend."""
    document_id = '28c0cacf-ed9f-415f-a6ce-88de1d0f4c18'
    document = Document(id=document_id)
    document.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    doc_id, stuff, file, content = result

    assert str(doc_id) == document_id
    assert stuff == document.stuff
    assert file == document.file
    assert content == document.content


@pytest.mark.integration
def test_save_document(dao):
    """Tests persistence object."""
    document_id = '28c0cacf-ed9f-415f-a6ce-88de1d0f4c18'
    document = Document(id=document_id, stuff='some content')
    document.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    doc_id, stuff, file, content = result

    assert str(doc_id) == document_id
    assert stuff == 'some content'
    assert file is None
    assert content is None

    # dao.nuke_database()


@pytest.mark.integration
def test_save_document_with_attachment(dao):
    """Tests persistence object and attachment."""
    document_id = '28c0cacf-ed9f-415f-a6ce-88de1d0f4c18'
    uri = 'milost-gadget-test-bucket/test.txt'

    document = Document(id=document_id, stuff='some content')
    document.attach('file', b'This is a test', uri)
    document.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    doc_id, stuff, file, content = result

    assert str(doc_id) == document_id
    assert stuff == 'some content'
    assert file['identifier'] is not None
    assert isinstance(file['identifier'], str)
    assert file['bucket'] == 'milost-gadget-test-bucket'
    assert file['path'] is None
    assert_that(file['prefix']).is_equal_to(['artifact', file['identifier']])
    assert file['filename'] == 'test.txt'
    assert file['artifact_name'] == f'artifact_{file["identifier"]}_-_test.txt'
    assert file['extension'] == 'txt'
    assert file['content_type'] == 'application/octet-stream'
    assert file['store_id'] == 'gcs'
    assert content is None

    assert dao.storage.bucket_exists('milost-gadget-test-bucket')
    assert f'milost-gadget-test-bucket/{file["artifact_name"]}' in dao.storage
    content = dao.storage.get(f'milost-gadget-test-bucket/{file["artifact_name"]}')
    assert content == b'This is a test'

    # dao.storage.delete_bucket('milost-gadget-test-bucket', force=True)
    # dao.nuke_database()


@pytest.mark.integration
def test_delete_attachment_from_backend(dao):
    """Tests deletion of an artifact from the backend."""
    document_id = '28c0cacf-ed9f-415f-a6ce-88de1d0f4c18'
    uri = 'milost-gadget-test-bucket/test.txt'

    document = Document(id=document_id, stuff='some content')
    document.attach('file', b'This is a test', uri)
    document.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    _, _, file, _ = result

    assert file is not None
    assert f'milost-gadget-test-bucket/{file["artifact_name"]}' in dao.storage

    # Delete artifact in external storage
    doc = Document.fetch(document_id)
    doc.file.delete()

    result = list(dao.session.execute(text("select * from documents")))[0]
    _, _, file, _ = result

    assert file is not None
    assert f'milost-gadget-test-bucket/{file["artifact_name"]}' not in dao.storage

    # dao.storage.delete_bucket('milost-gadget-test-bucket', force=True)
    # dao.nuke_database()


@pytest.mark.integration
def test_delete_attachment_via_set_None(dao):
    """Tests deletion of an artifact from the backend by setting it to None."""
    document_id = '28c0cacf-ed9f-415f-a6ce-88de1d0f4c18'
    uri = 'milost-gadget-test-bucket/test.txt'

    document = Document(id=document_id, stuff='some content')
    document.attach('file', b'This is a test', uri)
    document.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    _, _, file, _ = result

    artifact_name = file["artifact_name"]

    assert file is not None
    assert f'milost-gadget-test-bucket/{artifact_name}' in dao.storage

    # Delete artifact in external storage
    doc = Document.fetch(document_id)
    doc.file = None
    doc.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    _, _, file, _ = result

    assert file is None
    assert f'milost-gadget-test-bucket/{artifact_name}' not in dao.storage

    # dao.storage.delete_bucket('milost-gadget-test-bucket', force=True)
    # dao.nuke_database()


@pytest.mark.integration
def test_delete_attachment_and_keep_orphan(dao):
    """Tests deletion of an object in the database.

    Deletes an object from the database, but
    forgoes the deletion of the attachment from the
    external storage.
    """
    document_id = '28c0cacf-ed9f-415f-a6ce-88de1d0f4c18'
    uri = 'milost-gadget-test-bucket/test.txt'

    document = Document(id=document_id, stuff='some content')
    document.attach('file', b'This is a test', uri)
    document.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    _, _, file, _ = result

    artifact_name = file["artifact_name"]

    assert file is not None
    assert f'milost-gadget-test-bucket/{artifact_name}' in dao.storage

    # Delete artifact in external storage
    doc = Document.fetch(document_id)
    doc.file = None
    doc.save(keep_orphans=True)  # Keep orphaned attachments

    result = list(dao.session.execute(text("select * from documents")))[0]
    _, _, file, _ = result

    assert file is None
    assert f'milost-gadget-test-bucket/{artifact_name}' in dao.storage

    # dao.storage.delete_bucket('milost-gadget-test-bucket', force=True)
    # dao.nuke_database()


@pytest.mark.integration
def test_delete_document(dao):
    """Tests deletion of the entire object, including attachments."""
    document_id = '28c0cacf-ed9f-415f-a6ce-88de1d0f4c18'
    uri = 'milost-gadget-test-bucket/test.txt'

    document = Document(id=document_id, stuff='some content')
    document.attach('file', b'This is a test', uri)
    document.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    _, _, file, _ = result

    artifact_name = file["artifact_name"]

    assert file is not None
    assert f'milost-gadget-test-bucket/{artifact_name}' in dao.storage

    # Delete artifact in external storage
    doc = Document.fetch(document_id)
    doc.delete()

    result = list(dao.session.execute(text("select * from documents")))

    assert_that(result).is_empty()
    assert f'milost-gadget-test-bucket/{artifact_name}' not in dao.storage

    # dao.storage.delete_bucket('milost-gadget-test-bucket', force=True)
    # dao.nuke_database()


@pytest.mark.integration
def test_delete_document_keep_orphans(dao):
    """Tests the deletion of an object but keeps attachments."""
    document_id = '28c0cacf-ed9f-415f-a6ce-88de1d0f4c18'
    uri = 'milost-gadget-test-bucket/test.txt'

    document = Document(id=document_id, stuff='some content')
    document.attach('file', b'This is a test', uri)
    document.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    _, _, file, _ = result

    artifact_name = file["artifact_name"]

    assert file is not None
    assert f'milost-gadget-test-bucket/{artifact_name}' in dao.storage

    # Delete artifact in external storage
    doc = Document.fetch(document_id)
    doc.delete(keep_orphans=True)

    result = list(dao.session.execute(text("select * from documents")))

    assert_that(result).is_empty()
    assert f'milost-gadget-test-bucket/{artifact_name}' in dao.storage

    # dao.storage.delete_bucket('milost-gadget-test-bucket', force=True)
    # dao.nuke_database()


@pytest.mark.integration
def test_update_attached_file(dao):
    """Tests updating the content of an artifact."""
    document_id = '28c0cacf-ed9f-415f-a6ce-88de1d0f4c18'
    uri = 'milost-gadget-test-bucket/test.txt'

    document = Document(id=document_id, stuff='some content')
    document.attach('file', b'This is a test', uri)
    document.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    _, _, file, _ = result

    artifact_name = file["artifact_name"]

    assert file is not None
    assert f'milost-gadget-test-bucket/{artifact_name}' in dao.storage
    content = dao.storage.get(f'milost-gadget-test-bucket/{artifact_name}')
    assert content == b'This is a test'

    doc = Document.fetch(document_id)
    doc.file.replace(b'Overwriting the old data')

    assert file is not None
    assert f'milost-gadget-test-bucket/{artifact_name}' in dao.storage
    content = dao.storage.get(f'milost-gadget-test-bucket/{artifact_name}')
    assert content == b'Overwriting the old data'

    # dao.storage.delete_bucket('milost-gadget-test-bucket', force=True)
    # dao.nuke_database()


@pytest.mark.integration
def test_replace_attached_file(dao):
    """Tests the replacement of an attached artifact."""
    document_id = '28c0cacf-ed9f-415f-a6ce-88de1d0f4c18'
    uri = 'milost-gadget-test-bucket/test.txt'

    document = Document(id=document_id, stuff='some content')
    document.attach('file', b'This is a test', uri)
    document.save()

    result = list(dao.session.execute(text("select * from documents")))[0]
    _, _, file, _ = result

    artifact_name = file["artifact_name"]
    original_file_id = file["identifier"]

    assert file is not None
    assert f'milost-gadget-test-bucket/{artifact_name}' in dao.storage
    content = dao.storage.get(f'milost-gadget-test-bucket/{artifact_name}')
    assert content == b'This is a test'

    doc = Document.fetch(document_id)
    doc.update(file=Artifact.create(b'The cat with the hat.',
                                    uri='milost-gadget-test-bucket/dog.txt',
                                    attached_to=document.id))

    result = list(dao.session.execute(text("select * from documents")))[0]
    doc_id, stuff, file, content = result

    # Check if new artifact got stored
    assert str(doc_id) == document_id
    assert stuff == 'some content'
    assert content is None

    assert file is not None
    assert file['identifier'] is not None
    assert isinstance(file['identifier'], str)
    assert file['identifier'] != original_file_id
    assert file['bucket'] == 'milost-gadget-test-bucket'
    assert file['path'] is None
    assert_that(file['prefix']).is_equal_to(['artifact', file['identifier']])
    assert file['filename'] == 'dog.txt'
    assert file['artifact_name'] == f'artifact_{file["identifier"]}_-_dog.txt'
    assert file['extension'] == 'txt'
    assert file['content_type'] == 'application/octet-stream'
    assert file['store_id'] == 'gcs'
    assert f'milost-gadget-test-bucket/{file["artifact_name"]}' in dao.storage
    content = dao.storage.get(f'milost-gadget-test-bucket/{file["artifact_name"]}')
    assert content == b'The cat with the hat.'

    # check that old attachment got removed
    assert f'milost-gadget-test-bucket/{artifact_name}' not in dao.storage
