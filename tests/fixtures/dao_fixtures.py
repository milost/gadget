import os
import functools

import pytest
from sqlalchemy.engine import base

from gadget.dao import Connection, DAO
from gadget.storage.gcs import GCStorage

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker


@pytest.fixture
def engine() -> base.Engine:
    return create_engine(
        "postgresql://{}:{}@{}:{}/{}".format(
            os.environ.get('TEST_DB_USER', 'postgres'),
            os.environ.get('TEST_DB_PASSWORD', 'postgres123'),
            os.environ.get('TEST_DB_HOST', 'localhost'),
            os.environ.get('TEST_DB_PORT', 5432),
            os.environ.get('TEST_DB_NAME', 'postgres'),
        )
    )


@pytest.fixture
def session(engine):
    yield scoped_session(
        sessionmaker(autocommit=False, autoflush=False, bind=engine)
    )


@pytest.fixture
def dao():
    """Fixture that sets up the DAO object.

    Returns:
        a DAO object instance
    """
    gcs_storage = functools.partial(GCStorage)

    connection = Connection('localhost', 5432, 'postgres', 'postgres123', 'postgres')
    dao = DAO(connection)

    dao.register_storage('gcs', gcs_storage, default=True)
    dao.init()

    yield dao

    print("Flushing database")
    dao.storage.delete_bucket('milost-gadget-test-bucket', force=True)
    dao.nuke_database()
