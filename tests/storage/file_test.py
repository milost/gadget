# -*- coding: utf-8 -*-
"""Test that the file storage behaves basically as we'd expect."""
import os

from gadget.models.artifact import ArtifactLocation
from gadget.storage.file import FileStorage


def test_init():
    """Test init."""
    storage = FileStorage("tests")
    assert storage.base_dir.endswith("tests")


def test_put():
    """Test that we can put some data."""
    storage = FileStorage("tests")
    data = b"some test data"
    location = ArtifactLocation.from_uri("test-bucket/subdir/data.txt")
    storage.put(location, data)
    path = os.path.join(storage.base_dir, location.uri)
    print(path)
    assert os.path.exists(path)
    with open(path, "rb") as f:
        data2 = f.read()
    assert data == data2


def test_get():
    """Test that we can get some data."""
    storage = FileStorage("tests")
    data = b"some test data"
    location = ArtifactLocation.from_uri("test-bucket/subdir/data.txt")
    storage.put(location, data)
    path = os.path.join(storage.base_dir, location.uri)
    print(path)
    assert os.path.exists(path)
    data2 = storage.get(location)
    assert data == data2


def test_delete():
    """Delete three objects in two ways."""
    storage = FileStorage("tests")
    data = b"some test data"
    locations = []
    for i in range(3):
        location = ArtifactLocation.from_uri(f"test-bucket/subdir/data_{i}.txt")
        storage.put(location, data)
        locations.append(location)

    storage.delete(locations[0])
    path = os.path.join(storage.base_dir, locations[0].uri)
    assert not os.path.exists(path)

    storage.delete(locations[1:])
    for location in locations[1:]:
        path = os.path.join(storage.base_dir, location.uri)
        assert not os.path.exists(path)


def test_delete_bucket():
    """Delete the subdir, then the base dir."""
    storage = FileStorage("tests")
    data = b"some test data"
    location = ArtifactLocation.from_uri("test-bucket/subdir/subdir2/data.txt")
    storage.put(location, data)

    path = os.path.join(storage.base_dir, location.uri)
    assert os.path.exists(path)

    path2 = os.path.dirname(path)
    storage.delete_bucket(path2)
    assert not os.path.exists(path)
    assert not os.path.exists(path2)

    path3 = os.path.dirname(os.path.dirname(path2))
    assert os.path.exists(path3)
    storage.delete_bucket(path3)
    assert not os.path.exists(path3)

    assert not os.path.exists("tests/test-bucket")
