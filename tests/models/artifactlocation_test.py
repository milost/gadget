# -*- coding: utf-8 -*-
"""This module implements the tests for the gadget package."""
import pytest

from gadget.models import ArtifactLocation

# pylint: disable=use-a-generator,no-value-for-parameter


def test_constructor():
    """Testing the ArtifactLocation constructor."""
    location = ArtifactLocation(bucket='bucket_1', path='images', filename='photo.jpeg')

    assert location.path_separator == '/'
    assert location.bucket == 'bucket_1'
    assert location.path == 'images'
    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'photo.jpeg'
    assert location.extension == 'jpeg'
    assert location.artifact_path == 'images/photo.jpeg'
    assert isinstance(location.prefix, list) and len(location.prefix) == 0

    location = ArtifactLocation(bucket='bucket_1', filename='photo.jpeg')

    assert location.path_separator == '/'
    assert location.bucket == 'bucket_1'
    assert location.path is None
    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'photo.jpeg'
    assert location.extension == 'jpeg'
    assert location.artifact_path == 'photo.jpeg'
    assert isinstance(location.prefix, list) and len(location.prefix) == 0

    location = ArtifactLocation(bucket='bucket_1', prefix=['some'], filename='photo.jpeg')

    assert location.path_separator == '/'
    assert location.bucket == 'bucket_1'
    assert location.path is None
    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'some_-_photo.jpeg'
    assert location.extension == 'jpeg'
    assert location.artifact_path == 'some_-_photo.jpeg'
    assert all([a == b for a, b in zip(location.prefix, ['some'])])

    location = ArtifactLocation(bucket='bucket_1',
                                path='path/to/file',
                                prefix=['some'],
                                filename='photo.jpeg')

    assert location.path_separator == '/'
    assert location.bucket == 'bucket_1'
    assert location.path == 'path/to/file'
    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'some_-_photo.jpeg'
    assert location.extension == 'jpeg'
    assert location.artifact_path == 'path/to/file/some_-_photo.jpeg'
    assert all([a == b for a, b in zip(location.prefix, ['some'])])

    with pytest.raises(TypeError):
        ArtifactLocation(filename='photo.jpeg')

    with pytest.raises(TypeError):
        ArtifactLocation(bucket='bucket_1')


def test_artifact_name():
    """Testing the ArtifactLocation artifact_name property."""
    location = ArtifactLocation(bucket='bucket_1', path='images', filename='photo.jpeg')

    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'photo.jpeg'
    assert location.artifact_name == location.filename

    location = ArtifactLocation(bucket='bucket_1',
                                path='images',
                                prefix=['some'],
                                filename='photo.jpeg')

    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'some_-_photo.jpeg'


def test_artifact_path():
    """Testing the ArtifactLocation artifact_path property."""
    location = ArtifactLocation(bucket='bucket_1', filename='photo.jpeg')

    assert location.artifact_path == 'photo.jpeg'

    location = ArtifactLocation(bucket='bucket_1', path='images', filename='photo.jpeg')

    assert location.artifact_path == 'images/photo.jpeg'

    location = ArtifactLocation(bucket='bucket_1',
                                path='images',
                                prefix=['some'],
                                filename='photo.jpeg')

    assert location.artifact_path == 'images/some_-_photo.jpeg'

    location = ArtifactLocation(bucket='bucket_1',
                                path='images',
                                prefix=['so-me', 'info'],
                                filename='photo.jpeg')

    assert location.artifact_path == 'images/so-me_info_-_photo.jpeg'

    location = ArtifactLocation(bucket='bucket_1',
                                path='path/to/file',
                                prefix=['so-me', 'info'],
                                filename='photo.jpeg')

    assert location.artifact_path == 'path/to/file/so-me_info_-_photo.jpeg'


def test_to_string():
    """Testing the ArtifactLocation __str__ method."""
    location = ArtifactLocation(bucket='bucket_1', path='images', filename='photo.jpeg')

    assert str(location) == 'bucket_1/images/photo.jpeg'

    location = ArtifactLocation(bucket='bucket_1', filename='photo.jpeg')

    assert str(location) == 'bucket_1/photo.jpeg'

    location = ArtifactLocation(bucket='bucket_1',
                                path='images',
                                prefix=['info'],
                                filename='photo.jpeg')

    assert str(location) == 'bucket_1/images/info_-_photo.jpeg'

    location = ArtifactLocation(bucket='bucket_1',
                                path='images',
                                prefix=['some', 'info'],
                                filename='photo.jpeg')

    assert str(location) == 'bucket_1/images/some_info_-_photo.jpeg'

    location = ArtifactLocation(bucket='bucket_1',
                                path='images',
                                prefix=['so-me', 'info'],
                                filename='photo.jpeg')

    assert str(location) == 'bucket_1/images/so-me_info_-_photo.jpeg'


def test_from_uri():
    """Testing the ArtifactLocation from_uri method."""
    location = ArtifactLocation.from_uri('bucket_1/images/some_info_-_photo.jpeg')

    assert location.bucket == 'bucket_1'
    assert location.path == 'images'
    assert all([a == b for a, b in zip(location.prefix, ['some', 'info'])])
    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'some_info_-_photo.jpeg'
    assert location.extension == 'jpeg'

    location = ArtifactLocation.from_uri('bucket_1/images/some_info_abcd_-_photo.jpeg')

    assert location.bucket == 'bucket_1'
    assert location.path == 'images'
    assert all([a == b for a, b in zip(location.prefix, ['some', 'info', 'abcd'])])
    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'some_info_abcd_-_photo.jpeg'
    assert location.extension == 'jpeg'

    location = ArtifactLocation.from_uri('bucket_1/images/some_-_photo.jpeg')

    assert location.bucket == 'bucket_1'
    assert location.path == 'images'
    assert all([a == b for a, b in zip(location.prefix, ['some'])])
    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'some_-_photo.jpeg'
    assert location.extension == 'jpeg'

    location = ArtifactLocation.from_uri('bucket_1/images/photo.jpeg')

    assert location.bucket == 'bucket_1'
    assert location.path == 'images'
    assert all([a == b for a, b in zip(location.prefix, [])])
    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'photo.jpeg'
    assert location.artifact_name == location.filename
    assert location.extension == 'jpeg'

    location = ArtifactLocation.from_uri('bucket_1/photo.jpeg')

    assert location.bucket == 'bucket_1'
    assert location.path is None
    assert all([a == b for a, b in zip(location.prefix, [])])
    assert location.filename == 'photo.jpeg'
    assert location.artifact_name == 'photo.jpeg'
    assert location.artifact_name == location.filename
    assert location.extension == 'jpeg'
