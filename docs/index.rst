Welcome to the gadget documentation!
=====================================================

.. toctree::
   :maxdepth: 2

   others
   autoapi/index.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
