#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains a function to display the targets contained in a Makefile."""
import re
import sys

from rich.console import Console
from rich.table import Table


def main():
    """Displays the targets of a Makefile as multiple tables.

    This function receives the content of the Makefile as input
    and displays the make targets according to their groups in
    multiple tables.
    """
    console = Console()

    target_pattern = r"^(?!#)(.*):\s+(.*)##\s+(.*)"
    target_group_pattern = r"#!\s(.*?)\s#\n"
    split_pattern = r"#!\s.*?#\n"

    # read data
    data = sys.stdin.read()

    # split data according to target groups
    splits = re.split(split_pattern, data)
    splits = splits[1:]

    # extract targets that belong to one group
    targets = []
    for snippet in splits:
        matches = re.finditer(target_pattern, snippet, re.MULTILINE)
        targets.append([(match.group(1), match.group(3)) for match in matches])

    # extract target group itself
    matches = re.finditer(target_group_pattern, data, re.MULTILINE)
    target_groups = [match.group(1) for match in matches]

    # put togther table to display targets and their groups
    for target_group, targets in zip(target_groups, targets):
        table = Table(title=f'{target_group}', title_style='bold')
        table.add_column("Target", justify="left", style="bright_yellow", no_wrap=True)
        table.add_column("Description", justify="left", style="bright_yellow", no_wrap=True)
        for target, description in targets:
            table.add_row(target, description)
        console.print(table)


if __name__ == '__main__':
    main()
