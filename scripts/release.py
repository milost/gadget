#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""This module contains functions to create and accept Gitlab merge requests."""
import argparse
import re
import time

import requests
from git import Repo

parser = argparse.ArgumentParser(description="Create and accept merge request")
parser.add_argument("project_id",
                    type=int,
                    help="the project id for which a merge request should be created")
parser.add_argument("token", type=str, help="token for accessing the Gitlab API")


def create_merge_request(project_id, access_token, version):
    """Creates a gitlab merge request.

    Creates a merge request for the provided project_id from
    the develop to the master branch.

    Args:
        project_id: the project id for which the merge request should be created
        access_token: the access token used for connecting to gitlab
        version: the packages version that is currently being released

    Returns:
        a json object that contains information about the creates merge request
    """
    headers = {"Authorization": f"Bearer {access_token}"}

    payload = {
        "target_branch": "master",
        "source_branch": "develop",
        "title": f"RELEASE: Create new release v{version}",
        "description": f"RELEASE: Create new release v{version}",
    }

    print(f"https://gitlab.com/api/v4/projects/{project_id}/merge_requests")
    response = requests.post(f"https://gitlab.com/api/v4/projects/{project_id}/merge_requests",
                             headers=headers,
                             data=payload)

    return response.json()


def accept_merge_request(project_id, merge_request_id, access_token):
    """Accept a merge request.

    Accepts the merge request with the specified merge_request_id
    for the specified project

    Args:
        project_id: the project id containing the merge request
        merge_request_id: the id of the merge request that is to be accepted
        access_token: the access token used for connecting to gitlab
    """
    headers = {"Authorization": f"Bearer {access_token}"}

    payload = {"merge_when_pipeline_succeeds": True, "squash": False}

    requests.put(
        f"https://gitlab.com/api/v4/projects/{project_id}/merge_requests/{merge_request_id}/merge",
        headers=headers,
        data=payload,
    )


def main():
    """Creates and accepts a merge request for releasing the package."""
    args = parser.parse_args()

    # get current version
    repo = Repo(".")
    version = str(repo.tags[-1])
    version = re.search(r"([\d.]+)", version).group(1)
    print(f"Creating merge request for release {version}")

    # create merge request
    response = create_merge_request(args.project_id, args.token, version)
    merge_request_id = int(response["iid"])
    time.sleep(5)
    print(f"Accepting Merge Request #{merge_request_id}")
    accept_merge_request(args.project_id, merge_request_id, args.token)


if __name__ == "__main__":
    main()
