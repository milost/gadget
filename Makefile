define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT
BROWSER := python -c "$$BROWSER_PYSCRIPT"

#############################
# Include environment files #
#############################

EXTRA_INCLUDES:=$(wildcard .env*)
ifneq ($(strip $(EXTRA_INCLUDES)),)
  CONTENTS :=  $(shell echo including extra rules $(EXTRA_INCLUDES))
  include $(EXTRA_INCLUDES)
  export
endif

####################
# Define variables #
####################

# Setting PYTHONPATH to current directory is needed for the make run target
export PYTHONPATH=.

.DEFAULT_GOAL := help
SHELL:=/bin/bash
ENV:=$(or $(and $(wildcard ./.env),1),0) # Check if there exists a .env file
PYENV:=$(shell command -v pyenv)  # Check if pyenv command exists
PROJECT_NAME:=$(shell sed -n '/^NAME = "/s/.*"\(.*\)"/\1/p' < setup.py)
GITLAB_USER:=
PROJECT_ID:=32559173
PROJECT_DESCRIPTION:=An easy to use ORM implementation based on SQL Alchemy
PROJECT_REPOSITORY:=https://gitlab.com/milost/gadget
PROJECT_HOMEPAGE:=https://milost.gitlab.io/gadget
PACKAGE_VERSION:=$(shell sed -n '/^VERSION = "/s/.*"\(.*\)"/\1/p' < setup.py)
PYTHON_VERSION:=3.10.5
PACKAGE_REPOSITORY=artifactory
INSTALLED_PACKAGES:=$(shell pip list)
IS_GIT_REPO:=$(if $(filter .git, $(shell ls -la)),true,false)
RICH_EXISTS:=$(if $(filter rich, $(INSTALLED_PACKAGES)),true,false)
REQUIREMENTS:=$(shell cat requirements.txt)
DEV_REQUIREMENTS:=$(shell cat requirements-dev.txt)
ifneq ($(PYENV),)
	INSTALLED_PYTHON_VERSIONS:=$(shell pyenv versions)
	AVAILABLE_PYTHON_VERSIONS:=$(shell pyenv install -l)
	ENV_EXISTS:=$(if $(filter $(PROJECT_NAME), $(INSTALLED_PYTHON_VERSIONS)),true,false)
	IS_INSTALLABLE:=$(if $(filter $(PYTHON_VERSION), $(AVAILABLE_PYTHON_VERSIONS)),true,false)
	IS_INSTALLED:=$(if $(filter $(PYTHON_VERSION), $(INSTALLED_PYTHON_VERSIONS)), true,false)
endif

##################
#! Clean targets #
##################

.PHONY: clean-build
clean-build: ## Remove build artifacts
	@echo "+ $@"
	@rm -fr build/
	@rm -fr dist/
	@rm -fr *.egg-info

.PHONY: clean-pyc
clean-pyc: ## Remove python file artifacts
	@echo "+ $@"
	@find . -type d -name '__pycache__' -exec rm -rf {} +
	@find . -type f -name '*.py[co]' -exec rm -f {} +
	@find . -name '*~' -exec rm -f {} +

.PHONY: clean-test
clean-test: ## Remove all test artifacts
	@echo "+ $@"
	@rm -fr htmlcov
	@rm -fr .pytest_cache
	@rm -fr coverage.xml
	@rm -fr .coverage

.PHONY: clean
clean: clean-build clean-pyc clean-test ## Remove all artifacts
	@echo "+ $@"
	@rm -fr response.json
	@rm -fr .eggs
	@rm -fr .mypy_cache

##################
#! Setup targets #
##################

.PHONY: install-pyenv
install-pyenv: ## Install pyenv
	@echo "+ $@"
ifneq ($(wildcard ~/.pyenv),) # check if pyenv is already installed
	@echo "It seems that pyenv is already installed"
else
	@echo "Installing pyenv"
	@git clone https://github.com/pyenv/pyenv.git ~/.pyenv
	@git clone https://github.com/pyenv/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv
	@echo -e "pyenv successfully installed\n"
	@echo -e "Add the following lines to the end of your shell config:\n"
	@echo -e 'export PYENV_ROOT="$$HOME/.pyenv"'
	@echo -e 'export PATH="$$PYENV_ROOT/bin:$$PATH"'
	@echo "if command -v pyenv 1>/dev/null 2>&1; then"
	@echo 'eval "$$(pyenv init --path)"'
	@echo 'eval "$$(pyenv virtualenv-init -)"'
	@echo -e "fi\n"
	@echo "Don't forget to restart your shell!"
endif

.PHONY: install-python-version
install-python-version: # Installs the required python version using pyenv
ifeq ($(findstring $(PYTHON_VERSION),$(INSTALLED_PYTHON_VERSIONS)),)
	@echo "Installing Python version: $(PYTHON_VERSION) ..."
	@pyenv install $(PYTHON_VERSION)
else
	@echo "Python v$(PYTHON_VERSION) is already installed"
endif

.PHONY: drop-python-version
drop-python-version: # Deletes the required python version from pyenv
ifeq ($(findstring $(PYTHON_VERSION),$(INSTALLED_PYTHON_VERSIONS)),)
	@echo "Python v$(PYTHON_VERSION) does not exist ..."
else
	@echo "Uninstalling Python v$(PYTHON_VERSION) ..."
	pyenv uninstall -f $(PYTHON_VERSION)
endif

.PHONY: environment
environment: install-python-version ## Use pyenv to create a virtual environment
	@echo "+ $@"
	@pyenv virtualenv $(PYTHON_VERSION) $(PROJECT_NAME)
	@pyenv local $(PROJECT_NAME)

.PHONY: drop-environment
drop-environment: ## Delete pyenv environment
	@echo "+ $@"
	@pyenv virtualenv-delete -f $(PROJECT_NAME)
	@rm -f .python-version

.PHONY: initial-push
initial-push: # Initial push of the project to a git repo
	@git init
	@git remote add origin $(PROJECT_REPOSITORY)
	@git add .
	@git commit -m "Initial commit"
	@git push -u origin master
	@git checkout -b develop
	@git push -u origin develop

.PHONY: install-hooks
install-hooks: # Install local commit hooks
ifeq ($(IS_GIT_REPO), true)
	@pre-commit install --hook-type pre-commit --hook-type commit-msg
else
	@echo "No git repository initialized => skipping installation of hooks"
endif

.PHONY: install
install: ## Install project dependencies
	@echo "+ $@"
	@pip install --upgrade pip
ifeq ($(env), dev)
	@echo "Installing project dependencies including development dependencies ..."
	@pip install -r requirements-dev.txt
	@$(MAKE) install-hooks
else
	@echo "Installing project dependencies ..."
	@pip install -r requirements.txt
endif

.PHONY: activate
activate: ## Activate project environment
	@pyenv activate

.PHONY: update
update: ## Update project dependencies
	@echo "+ $@"
	@pip install --upgrade pip
ifeq ($(env), dev)
	@echo "Updating project dependencies including development dependencies ..."
	@pip install -U -r requirements-dev.txt
else
	@echo "Updating project dependencies ..."
	@pip install -U -r requirements.txt
endif

.PHONY: init
init: ## Initialize the project setup
	@echo "+ $@"
	@$(MAKE) environment
ifeq ($(for), development)
	@echo "Setting up project for development"
	@$(MAKE) install env=dev
else
	@$(MAKE) install
endif

########################
#! Development targets #
########################

.PHONY: run
run: ## Run main method of python project
	@echo "+ $@"
	@python ./$(PROJECT_NAME)/main.py

.PHONY: format
format: ## Format code with yapf
	@echo "+ $@"
	@python -m yapf --in-place --recursive --verbose ./*.py test/*.py synappse/*.py

.PHONY: analyze
analyze: ## Run full analysis
	@echo "+ $@"
	@pre-commit run --all-files

####################
#! Release targets #
####################

.PHONY: release
release: ## Increment package version
	@echo "+ $@"
ifneq ($(version),)
	@echo "Building release $(version)"
	@cz bump --changelog --check-consistency --increment $(version)
else
	@cz bump --changelog --check-consistency
endif
	@echo "Push changes made for new package version"
	@git push
	@git push --tags
	@./scripts/release.py $(PROJECT_ID) $(GITLAB_PRIVATE_TOKEN)

.PHONY: commit
commit: ## Commit changes using commitizen
	@cz commit

##################
#! Build targets #
##################

.PHONY: build
build: clean-build ## Build source and wheels archives
	@echo "+ $@"
	@python setup.py sdist bdist_wheel --universal
	@ls -l dist


.PHONY: sdist
sdist: clean-build ## Build sdist distribution
	@echo "+ $@"
	@python setup.py sdist
	@ls -l dist


.PHONY: wheel
wheel: clean-build ## Build bdist_wheel distribution
	@echo "+ $@"
	@python setup.py bdist_wheel --universal
	@ls -l dist

.PHONY: publish
publish: build ## Publish packages to python repository
	@echo "Publishing packages to $(PACKAGE_REPOSITORY) repository."
	@twine upload -r $(PACKAGE_REPOSITORY) ./dist/* --config-file ~/.pypirc

.PHONY: docs
docs: ## Generate Sphinx HTML documentation, including API docs
	@echo "+ $@"
	@$(MAKE) -C ./docs clean
	@$(MAKE) -C ./docs html
ifneq ($(show),)
	@$(BROWSER) docs/_build/html/index.html
endif

.PHONY: serve-docs
serve-docs: ## Serve the documentation on 0.0.0.0, used in tandem with `make docs`
	@echo "+ $@"
	@python -m http.server --bind 0.0.0.0 8000 --directory ./docs/_build/html/

#################
#! Test targets #
#################

.PHONY: test
test: ## Runs unit tests
	@echo "+ $@"
ifeq ($(coverage), true)
	@pytest -m "not integration" --cov-report html --cov-report xml --cov=tests/
ifeq ($(show), true)
	@$(BROWSER) htmlcov/index.html
endif
else
	@pytest -m "not integration"
endif

.PHONY: integration-test
integration-test: ## Runs integration tests
	@echo "+ $@"
ifeq ($(coverage), true)
	@pytest -m integration --cov-report html --cov-report xml --cov=tests/
ifeq ($(show), true)
	@$(BROWSER) htmlcov/index.html
endif
else
	@pytest -m integration
endif

###########################
#! Infrastructure targets #
###########################

.PHONY: start-postgres
start-postgres: ## Starts a local postgres server
	@echo "Starting postgres @ Host: localhost, Port: 5432"
	@echo "Username: postgres"
	@echo "Password: postgres123"
	@docker-compose up --detach postgres

.PHONY: stop-postgres
stop-postgres: ## Stops local postgres server
	@docker container stop postgres

.PHONY: destroy-postgres
destroy-postgres: ## Remove local postgres container
	@docker container rm postgres


.PHONY: start-minio
start-minio: ## Starts a local minio server
	@echo "Starting minio @ Host: localhost, Ports: 9000, 9001"
	@echo "Username: minio"
	@echo "Password: minio123"
	@docker-compose up --detach minio

.PHONY: stop-minio
stop-minio: ## Stops local minio server
	@docker container stop minio

.PHONY: destroy-minio
destroy-minio: ## Destroy local containers
	@docker container rm minio

.PHONY: info
info: # Target for testing purposes
	@echo "+ $@"
	@echo "PROJECT_NAME=$(PROJECT_NAME)"
	@echo "PYENV=$(PYENV)"
	@echo "PIPENV=$(PIPENV)"
	@echo "ENV=$(ENV)"
	@echo "PYTHON_VERSION=$(PYTHON_VERSION)"
	@echo "PACKAGE_VERSION=$(PACKAGE_VERSION)"
	@echo "EXTRA_INCLUDES=$(EXTRA_INCLUDES)"
	@echo "PROJECT_ENV=$(PROJECT_ENV)"
	@echo "MAKEFILE_LIST=$(MAKEFILE_LIST)"
	@echo "REQUIRED_PYTHON_VERSION=$(REQUIRED_PYTHON_VERSION)"
	@echo "INSTALLED_PYTHON_VERSIONS=$(INSTALLED_PYTHON_VERSIONS)"
	@echo "AVAILABLE_PYTHON_VERSIONS=$(AVAILABLE_PYTHON_VERSIONS)"
	@echo "IS_INSTALLABLE=$(IS_INSTALLABLE)"
	@echo "IS_INSTALLED=$(IS_INSTALLED)"
	@echo "RICH_EXISTS=$(RICH_EXISTS)"
	@echo "IS_GIT_REPO=$(IS_GIT_REPO)"

.PHONY: help
help:
ifeq ($(RICH_EXISTS), true)
	@cat Makefile | ./scripts/display_targets.py
else
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
endif
