## v0.3.2 (2022-03-01)

### Fix

- **mixin**: fix reconstruction of loaded objects

## v0.3.1 (2022-02-11)

### Fix

- **DAO**: replace close sessions call
- fix bvgs
- FileStorage.get incorrect return type
- FileStorage.get incorrect return type
- make FileStorage work with ArtifactLocation and update tests that didn't catch the bugs
- make FileStorage work with ArtifactLocation and update tests that didn't catch the bugs

## v0.3.0 (2022-02-04)

### Refactor

- **storage**: adapt list of file storage new interface
- **models**: fix imports
- **storage**: restructure package exports
- **storage**: fix imports to prevent circular imports

### Fix

- **mixin**: fix import

### Feat

- add FileStorage backend and some tests
- add FileStorage backend and some tests
- **gcs**: add capability to store metadata with GCS
- **artifact**: add content type and artifact path
- **storage**: add Google Cloud Storage backend
- **exception**: add exception for artifact not found
- add google-cloud-storage dependency

## v0.2.0 (2022-01-27)

### Feat

- **mixin**: Rewrite delete method
- **model**: add artifacts and artifact locations
- **mixin**: add attach method to attachment mixin
- **dao**: add functionality to register storage backends
- **mixin**: add attachment mixin
- **storage**: add storage manager
- **storage**: add module exports to storage package
- **storage**: add implementation of S3 storage backend
- **storage**: add interface for storage backends
- **dao**: add function that returns pg connection string

### Refactor

- **model**: refactor the artifact model

### Fix

- **storage**: fix bug in delete method of s3 backend
- delete, contains and metadata methods
- prevent reorder of import
- **linter**: stop checking for duplicate code snipptes
- **activate_record**: call to flush is not needed

## v0.1.0 (2022-01-17)

### Refactor

- **main**: main is not needed
- **main**: condense main file
- **AR**: refactor active record module
- **query**: remove query module
- **gadget**: use active record mixin as default model
- **export**: export mixin classes
- **main**: conduct additional experiments
- **mixin**: export serializable and inspection mixin
- **main**: basic functionality experiments
- **init**: add default model as module export
- **mixins**: Add SessionMixin as package export
- **main**: Simplify main.py
- Delete unneeded files
- Delete unneeded files

### Feat

- **type**: add caching to GUID type
- **session**: add commit method
- **active_record**: add method to access all objects
- **mixin**: add active record mixin
- **mixin**: add smart query mixin
- **mixin**: add eager loading mixin
- **mixin**: add inspection mixin
- **mixin**: add serializable mixin
- **dao**: add data access object
- **mixin**: create mixin module
- **mixin**: add session mixin
- **extension**: add sqlalchemy type extensions
- **DAO**: add data access object
- **session**: add SessionMixin module

### Fix

- **linting**: disable checks
