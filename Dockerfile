# syntax = docker/dockerfile:1.0-experimental

# Use an official Python runtime as a parent image
FROM python:3.9-slim-buster

ARG package_name

# Define environment variables
ENV package_name=$package_name \
    ANOTHERVAR="another value"

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY gadget/  /app/gadget
COPY resources/ /app/resources
COPY LICENSE.md requirements-lock.txt setup.py /app/

## Install any needed packages specified in requirements-lock.txt
# RUN printf $NEXUS_CONFIG | base64 -d > /etc/pip.conf
RUN pip install --upgrade pip
RUN pip install -r requirements-lock.txt

# Run app.py when the container launches
CMD python ./gadget/main.py
