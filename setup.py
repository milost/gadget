#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""The setup script is the central place for building, distributing, and installing modules.

The setup script is the centre of all activity in building, distributing, and installing modules
using the Distutils. The main purpose of the setup script is to describe the module distribution
to the Distutils, so that the various commands that operate on your modules do the right thing.
"""
import io
import os
import re

from setuptools import find_packages
from setuptools import setup


def extract_version(text: str) -> dict:
    """Function that extracts a version number from a string.

    This function takes a python file in the form of a string and tries to extract a version number
    from the file contents (__version__). A dictionary with the major, minor and patch
    level version information is returned.

    Args:
        text: A string from which a version number is to be extracted

    Returns:
        A dictionary with the major, minor and patch level version information is returned.
    """
    regex = r"__version__\s=\s'(\d+)\.(\d+)\.(\d+)'"
    res = {}
    matches = re.finditer(regex, text, re.MULTILINE)
    for _, match in enumerate(matches, start=1):
        res['major'] = match.group(1)
        res['minor'] = match.group(2)
        res['patch'] = match.group(3)
    return res


def read_requirements(file: str):
    """Read dependencies from requirements.txt file."""
    packages = []
    try:
        with open(file, mode='r', encoding="utf-8") as s:
            for line in s:
                line = line.strip()
                if not line.startswith('-r'):
                    packages.append(line)
    except FileNotFoundError:
        pass

    return packages


# Package meta-data.
NAME = "gadget"
DESCRIPTION = "An easy to use ORM implementation based on SQL Alchemy"
HOMEPAGE = "https://milost.gitlab.io/gadget"
REPOSITORY = "https://gitlab.com/milost/gadget"
DOWNLOAD = "https://gitlab.com/milost/gadget/-/releases"
BUG_TRACKER = "https://gitlab.com/milost/gadget/-/issues"
DOCUMENTATION = "https://milost.gitlab.io/gadget"
EMAIL = "michael@loster.io"
AUTHOR = "Michael Loster"
README = "README.md"
REQUIRES_PYTHON = ">=3.9"
PYTHON_VERSION = "3.10.5"
VERSION = "0.3.2"
LICENSE = "MIT"

# Specify the packages that are required by this module.
REQUIRED = read_requirements('requirements.txt')

# Define packages that are optional.
EXTRAS = {'dev': read_requirements('requirements-dev.txt')}

# The rest you shouldn't have to touch too much :)
# ------------------------------------------------
# Except, perhaps the License and Trove Classifiers!
# If you do change the License, remember to change the Trove Classifier for that!

here = os.path.abspath(os.path.dirname(__file__))

# Import the README and use it as the long-description.
# Note: this will only work if 'README.md' is present in your MANIFEST.in file!
try:
    with io.open(os.path.join(here, README), encoding='utf-8') as f:
        long_description = f'\n{f.read()}'
except FileNotFoundError:
    long_description = DESCRIPTION

# Load the package's __version__.py module as a dictionary.
about = {}
if not VERSION:
    project_slug = NAME.lower().replace("-", "_").replace(" ", "_")
    with open(os.path.join(here, project_slug, '__init__.py'), mode='r', encoding="utf-8") as f:
        version = extract_version(f.read())
        about['__version__'] = f'{version["major"]}.{version["minor"]}.{version["patch"]}'
else:
    about['__version__'] = VERSION

setup(
    name=NAME,
    version=about['__version__'],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/markdown; charset=UTF-8',
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=HOMEPAGE,
    download_url=DOWNLOAD,
    project_urls={
        "Bug Tracker": BUG_TRACKER,
        "Documentation": DOCUMENTATION,
        "Source Code": REPOSITORY,
    },
    packages=find_packages(exclude=["tests", "*.tests", "*.tests.*", "tests.*"]),
    # If your package is a single module, use this instead of 'packages':
    # py_modules=['mypackage'],
    # package_data={'gadget': ['Pipfile']},
    include_package_data=True,

    # entry_points={
    #     'console_scripts': ['mycli=mymodule:cli'],
    # },
    install_requires=REQUIRED,
    extras_require=EXTRAS,
    license=LICENSE,
    test_suite='pytest',
    setup_requires=['pytest-runner', 'flake8'],
    tests_require=['pytest'],
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy'
    ],
    # $ setup.py [command] support.
    cmdclass={},
)
